# Book Lists

The goal of this project is to implement some application features, in order
to showcase my understanding of web development practices to potential employers.

The domain of this application will include:

- **users**
    - will sign up to the application
    - create book lists, and wishlists

- **roles**
    - will describe what actions a user is permitted  
      to perform within the application
    - initial roles will include:
        - **staff** - manages *books*, *authors*, and *tags*
        - **visitor** - creates *book_reviews* , *book_lists*, and *wishlists*

- **books**
    - the main resource of our application 
    - will often be included as an item within various lists

- **authors**
    - each book can have multiple authors associated
    - users will be able to search for books by author

- **tags**
    - each book can have multiple tags,
    - users will be able to search for books by tags

- **book_reviews**
    - each user can leave one review on any book they like
    - users can see all reviews written by others

- **book_lists**
    - each book_list belongs to only one user, and may contains multiple books

- **wishlists**
    - similar to *book_lists* but are used to describe the intention of purchasing new books
    - wishlist items will have links to online stores, where desired books may be purchased

# Setup Instructions

This README would normally document whatever steps are necessary to get the
application up and running.

Things you may want to cover:

* Ruby version
The used Ruby version is `ruby-3.0.2`.

* System dependencies

* Configuration

* Database creation
Use `rails db:create` to create a new Postgresql database named `book_listst_<RAILS_ENV>`.

* Database initialization

* How to run the test suite

* Services (job queues, cache servers, search engines, etc.)

* Deployment instructions

* ...
