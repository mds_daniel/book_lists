class AddTsvectorColumnToBooks < ActiveRecord::Migration[6.1]
  def change
    add_column :books, :tsv, :tsvector
    add_index :books, :tsv, using: :gin

    reversible do |dir|
      dir.up do
        execute <<-SQL
          CREATE TRIGGER books_tsv_trigger
          BEFORE INSERT OR UPDATE ON books
          FOR EACH ROW EXECUTE FUNCTION
          tsvector_update_trigger(tsv, 'pg_catalog.english', title);

          UPDATE books SET tsv = ''::tsvector
        SQL
      end

      dir.down do
        execute <<-SQL
          DROP TRIGGER books_tsv_trigger ON books;
        SQL
      end
    end
  end
end
