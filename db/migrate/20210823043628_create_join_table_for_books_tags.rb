class CreateJoinTableForBooksTags < ActiveRecord::Migration[6.1]
  def change
    create_join_table :books, :tags

    add_index :books_tags, :book_id
    add_index :books_tags, :tag_id
    add_index :books_tags, [:book_id, :tag_id], unique: true

    # When books are deleted, remove entry in join table
    add_foreign_key :books_tags, :books, on_delete: :cascade

    # When tags are deleted, remove entry in join table
    add_foreign_key :books_tags, :tags, on_delete: :cascade
  end
end
