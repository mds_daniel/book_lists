class CreateAuthorsBooksJoinTable < ActiveRecord::Migration[6.1]
  def change
    create_join_table :authors, :books

    add_index :authors_books, :author_id
    add_index :authors_books, :book_id
    add_index :authors_books, [:author_id, :book_id], unique: true

    # Do not allow authors to be deleted, when they have books assigned
    add_foreign_key :authors_books, :authors, on_delete: :restrict

    # When books are deleted, remove entry in join table
    add_foreign_key :authors_books, :books, on_delete: :cascade
  end
end
