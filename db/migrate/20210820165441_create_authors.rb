class CreateAuthors < ActiveRecord::Migration[6.1]
  def change
    create_table :authors do |t|
      t.string :name, null: false
      t.text :bio
      t.string :wiki_url

      t.timestamps
    end

    add_index :authors, :name, unique: true
  end
end
