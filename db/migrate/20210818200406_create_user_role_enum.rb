class CreateUserRoleEnum < ActiveRecord::Migration[6.1]
  def up
    create_enum 'user_role', ['visitor', 'staff']
  end

  def down
    drop_enum 'user_role'
  end
end
