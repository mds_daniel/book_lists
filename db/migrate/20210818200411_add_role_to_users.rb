class AddRoleToUsers < ActiveRecord::Migration[6.1]
  def change
    change_table :users do |t|
      t.enum :role, as: 'user_role', default: 'visitor'
    end
  end
end
