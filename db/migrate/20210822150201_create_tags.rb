class CreateTags < ActiveRecord::Migration[6.1]
  def change
    create_table :tags do |t|
      t.string :name, null: false
      t.string :icon
      t.string :color

      t.timestamps
    end

    add_index :tags, :name, unique: true
  end
end
