class CreateBookReviews < ActiveRecord::Migration[6.1]
  def change
    create_table :book_reviews do |t|
      t.belongs_to :book, null: false, index: true, foreign_key: { on_delete: :cascade }
      t.belongs_to :user, null: false, index: true, foreign_key: { on_delete: :cascade }
      t.text :body, null: false

      t.timestamps
    end

    add_index :book_reviews, [:book_id, :user_id], unique: true
  end
end
