require 'rails_helper'

RSpec.describe BookReviewPolicy do
  subject { BookReviewPolicy.new(user, book_review) }

  let(:other_user) { FactoryBot.create(:user) }
  let(:book) { FactoryBot.create(:book) }
  let(:book_review) { FactoryBot.create(:book_review, user: other_user, book: book) }

  context 'when not logged in' do
    let(:user) { nil }

    it { is_expected.to permit_action(:show) }
    it { is_expected.to forbid_actions([:create, :update, :destroy]) }
  end

  context 'when logged in as a regular user' do
    let(:user) { FactoryBot.create(:user) }

    it { is_expected.to permit_actions([:show, :create]) }
    it { is_expected.to forbid_actions([:update, :destroy]) }

    context 'when book review belongs to user' do
      let(:book_review) { FactoryBot.create(:book_review, user: user, book: book) }

      it { is_expected.to permit_actions([:update, :destroy]) }
    end
  end

  context 'when logged in as a staff member' do
    let(:user) { FactoryBot.create(:user, :staff) }

    it { is_expected.to permit_actions([:show, :create]) }

    # Staff members are not allowed to modify or delete other users reviews
    it { is_expected.to forbid_actions([:update, :destroy]) }

    context 'when book review belongs to user' do
      let(:book_review) { FactoryBot.create(:book_review, user: user, book: book) }

      it { is_expected.to permit_actions([:update, :destroy]) }
    end
  end

  context 'when logged in as an admin' do
    let(:user) { FactoryBot.create(:user, :admin) }

    it { is_expected.to permit_actions([:show, :create]) }

    # Admins are not allowed to modify other users reviews
    it { is_expected.to forbid_action(:update) }

    it { is_expected.to permit_action(:destroy) }

    context 'when book review belongs to user' do
      let(:book_review) { FactoryBot.create(:book_review, user: user, book: book) }

      it { is_expected.to permit_actions([:update, :destroy]) }
    end
  end
end
