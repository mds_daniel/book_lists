require 'rails_helper'

RSpec.describe TagPolicy do
  subject { TagPolicy.new(user, tag) }

  let(:tag) { FactoryBot.create(:tag) }

  context 'when not logged in' do
    let(:user) { nil }

    it { is_expected.to permit_action(:show) }
    it { is_expected.to forbid_actions([:create, :update, :destroy]) }
  end

  context 'when logged in as a regular user' do
    let(:user) { FactoryBot.create(:user) }

    it { is_expected.to permit_action(:show) }
    it { is_expected.to forbid_actions([:create, :update, :destroy]) }
  end

  context 'when logged in as a staff member' do
    let(:user) { FactoryBot.create(:user, :staff) }

    it { is_expected.to permit_action(:show) }
    it { is_expected.to permit_actions([:create, :update]) }
    it { is_expected.to forbid_action(:destroy) }
  end

  context 'when logged in as an admin' do
    let(:user) { FactoryBot.create(:user, :admin) }

    it { is_expected.to permit_action(:show) }
    it { is_expected.to permit_actions([:create, :update, :destroy]) }
  end
end
