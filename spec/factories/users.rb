FactoryBot.define do
  factory :user do
    email { Faker::Internet.safe_email }
    password { 'supersecret' }

    trait :staff do
      role { 'staff' }
    end

    trait :admin do
      admin { true }
    end
  end
end
