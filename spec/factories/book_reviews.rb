FactoryBot.define do
  factory :book_review do
    body { Faker::Lorem.paragraph(sentence_count: 10) }
  end
end
