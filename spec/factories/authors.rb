FactoryBot.define do
  factory :author do
    name { Faker::Book.author }
    bio { Faker::Lorem.paragraph }
    wiki_url { "https://en.wikipedia.org/wiki/#{Faker::Internet.uuid}" }
  end
end
