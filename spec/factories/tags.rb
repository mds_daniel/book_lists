# frozen_string_literal: true

FactoryBot.define do
  factory :tag do
    sequence(:name) { |n| "#{Faker::Book.genre}-#{n}" }
    color { Faker::Color.hex_color }
    icon { IconViewHelper.tag_icon }
  end
end
