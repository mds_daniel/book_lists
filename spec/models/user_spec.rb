require 'rails_helper'
require_relative '../support/validation_error_matcher.rb'

RSpec.describe User, type: :model do
  describe 'validation' do
    it 'returns true for valid record' do
      user = User.new(email: 'test@example.com', password: 'supersecret')

      expect(user).to be_valid
    end

    it 'returns errors when email is blank' do
      user = User.new(email: "", password: 'supersecret')

      expect(user).not_to be_valid
      expect(user).to have_validation_error(:email, "can\'t be blank")
    end

    it 'returns errors when email has invalid format' do
      user = User.new(email: "invalid", password: 'supersecret')

      expect(user).not_to be_valid
      expect(user).to have_validation_error(:email, "is invalid")
    end

    it 'returns errors when email already taken' do
      user = User.new(email: 'test@example.com', password: 'supersecret')

      # Create user with same email
      FactoryBot.create(:user, email: user.email)

      expect(user).not_to be_valid
      expect(user).to have_validation_error(:email, 'has already been taken')
    end

    it 'returns errors when password is missing' do
      user = User.new(email: 'test@example.com')

      expect(user).not_to be_valid
      expect(user).to have_validation_error(:password, "can\'t be blank")
    end
  end

  describe 'admin?' do
    it 'returns false for regular users' do
      user = FactoryBot.build(:user)

      expect(user).not_to be_admin
    end

    it 'returns true for admins' do
      admin = FactoryBot.build(:user, :admin)

      expect(admin).to be_admin
    end
  end

  describe 'role' do
    it 'is visitor for regular users' do
      user = FactoryBot.create(:user)

      expect(user.role).to eq('visitor')
    end

    it 'is staff for admins' do
      admin = FactoryBot.create(:user, :admin)

      expect(admin.role).to eq('staff')
    end
  end
end
