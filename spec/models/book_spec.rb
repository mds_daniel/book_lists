require 'rails_helper'
require_relative '../support/validation_error_matcher.rb'

RSpec.describe Book, type: :model do
  describe 'validation' do
    it 'returns true for valid record' do
      book = Book.new(
        title: 'The Colour of Magic',
        description: 'An awesome fantasy book from the Discworld series.'
      )

      expect(book).to be_valid
    end

    it 'returns errors when title is blank' do
      book = Book.new(
        title: '', # blank title
        description: 'An awesome fantasy book from the Discworld series.'
      )

      expect(book).not_to be_valid
      expect(book).to have_validation_error(:title, "can\'t be blank")
    end

    it 'returns errors when description is blank' do
      book = Book.new(
        title: 'The Colour of Magic',
        description: '' # blank description
      )

      expect(book).not_to be_valid
      expect(book).to have_validation_error(:description, "can\'t be blank")
    end

    it 'returns errors when description is too short' do
      book = Book.new(
        title: 'The Colour of Magic',
        description: 'a' # too short description
      )

      expect(book).not_to be_valid
      expect(book).to have_validation_error(:description, "is too short (minimum is 10 characters)")
    end
  end

  describe 'scopes' do
    describe 'search_for_index' do
      it 'returns books in alphabetical order' do
        book_b = FactoryBot.create(:book, title: 'Book B')
        book_a = FactoryBot.create(:book, title: 'Book A')
        book_c = FactoryBot.create(:book, title: 'Book C')

        books = Book.search_for_index.to_a

        expect(books.pluck(:id)).to eq([book_a.id, book_b.id, book_c.id])
      end

      it 'allows fulltext_search' do
        book_a = FactoryBot.create(:book, title: 'The Color Purple')
        book_b = FactoryBot.create(:book, title: 'The Color of Magic')
        book_c = FactoryBot.create(:book, title: 'Magic the Gathering: The Song of Time')

        magic_titles = Book.search_for_index(title: 'magic').pluck(:title)

        expect(magic_titles).to contain_exactly(
          'Magic the Gathering: The Song of Time',
          'The Color of Magic'
        )
      end

      it 'allows searching by authors' do
        author_a = FactoryBot.create(:author)
        author_b = FactoryBot.create(:author)
        author_c = FactoryBot.create(:author)

        book_a = FactoryBot.create(:book, title: 'Book A', authors: [author_a, author_c])
        book_b = FactoryBot.create(:book, title: 'Book B', authors: [author_a, author_b])
        book_c = FactoryBot.create(:book, title: 'Book C', authors: [author_c])

        expect(search_for_authors(author_a)).to eq([book_a, book_b])
        expect(search_for_authors(author_b)).to eq([book_b])
        expect(search_for_authors(author_c)).to eq([book_a, book_c])

        expect(search_for_authors([author_a, author_b])).to eq([book_a, book_b])
        expect(search_for_authors([author_a, author_c])).to eq([book_a, book_b, book_c])
        expect(search_for_authors([author_a, author_b, author_c])).to eq([book_a, book_b, book_c])
      end

      def search_for_authors(author_or_authors)
        author_ids = Array.wrap(author_or_authors).pluck(:id)
        Book.search_for_index(author_ids: author_ids)
      end

      it 'allows searching by tags' do
        tag_a = FactoryBot.create(:tag)
        tag_b = FactoryBot.create(:tag)
        tag_c = FactoryBot.create(:tag)

        book_a = FactoryBot.create(:book, title: 'Book A', tags: [tag_a, tag_c])
        book_b = FactoryBot.create(:book, title: 'Book B', tags: [tag_a, tag_b])
        book_c = FactoryBot.create(:book, title: 'Book C', tags: [tag_c])

        expect(search_for_tags(tag_a)).to eq([book_a, book_b])
        expect(search_for_tags(tag_b)).to eq([book_b])
        expect(search_for_tags(tag_c)).to eq([book_a, book_c])

        expect(search_for_tags([tag_a, tag_b])).to eq([book_a, book_b])
        expect(search_for_tags([tag_a, tag_c])).to eq([book_a, book_b, book_c])
        expect(search_for_tags([tag_a, tag_b, tag_c])).to eq([book_a, book_b, book_c])
      end

      def search_for_tags(tag_or_tags)
        tag_ids = Array.wrap(tag_or_tags).pluck(:id)
        Book.search_for_index(tag_ids: tag_ids)
      end

      it 'allows searching by title and author' do
        author_a = FactoryBot.create(:author)
        author_b = FactoryBot.create(:author)
        author_c = FactoryBot.create(:author)

        book_a = FactoryBot.create(:book, title: 'The Color Purple', authors: [author_a])
        book_b = FactoryBot.create(:book, title: 'The Color of Magic', authors: [author_b])
        book_c = FactoryBot.create(:book, title: 'Magic the Gathering: The Song of Time', authors: [author_c])

        # Search for `magic` by `author_c`
        magic_books_author_c = Book.search_for_index(title: 'magic', author_ids: [author_c.id])

        expect(magic_books_author_c.pluck(:title)).to contain_exactly(
          'Magic the Gathering: The Song of Time',
        )

        # Search for `magic` by `author_b`
        magic_books_author_b = Book.search_for_index(title: 'magic', author_ids: [author_b.id])

        expect(magic_books_author_b.pluck(:title)).to contain_exactly(
          'The Color of Magic'
        )

        # Search `color` by `author_c`
        color_books_author_c = Book.search_for_index(title: 'color', author_ids: [author_c.id])
        expect(color_books_author_c).to be_empty

        # Search `color` by `author_a`
        color_books_author_a = Book.search_for_index(title: 'color', author_ids: [author_a.id])
        expect(color_books_author_a.pluck(:title)).to contain_exactly('The Color Purple')

        # Search `color` by `author_b`
        color_books_author_b = Book.search_for_index(title: 'color', author_ids: [author_b.id])
        expect(color_books_author_b.pluck(:title)).to contain_exactly('The Color of Magic')

        # Search `color` books by both `author_a` and `author_b
        color_books_authors_a_b = Book.search_for_index(title: 'color', author_ids: [author_a.id, author_b.id])
        expect(color_books_authors_a_b.pluck(:title)).to contain_exactly(
          'The Color Purple',
          'The Color of Magic',
        )
      end
    end

    describe 'fulltext_search' do
      let!(:book_b) { FactoryBot.create(:book, title: 'The Color of Magic') }
      let!(:book_a) { FactoryBot.create(:book, title: 'The Color Purple') }
      let!(:book_c) { FactoryBot.create(:book, title: 'Magic the Gathering: The Song of Time') }

      it 'returns books which match text search' do
        # Search for 'magic'
        magic_titles = Book.fulltext_search('magic').pluck(:title).sort

        expect(magic_titles).to contain_exactly(
          'Magic the Gathering: The Song of Time',
          'The Color of Magic'
        )

        color_titles = Book.fulltext_search('color').pluck(:title).sort

        expect(color_titles).to contain_exactly(
          'The Color of Magic',
          'The Color Purple'
        )

        # Search for both 'color' and 'magic'
        magic_color_titles = Book.fulltext_search('color magic').pluck(:title).sort

        expect(magic_color_titles).to contain_exactly(
          'The Color of Magic'
        )
      end
    end
  end

  describe 'reviewed_by?' do
    let(:book) { FactoryBot.create(:book) }
    let(:user) { FactoryBot.create(:user) }

    it 'returns true once user has reviewed book' do
      expect(book.reviewed_by?(user)).to eq(false)

      book.book_reviews.create!(FactoryBot.attributes_for(:book_review, user: user))

      expect(book.reviewed_by?(user)).to eq(true)
    end
  end
end
