require 'rails_helper'
require_relative '../support/validation_error_matcher.rb'

RSpec.describe Tag, type: :model do
  describe 'validations' do
    let(:valid_attrs) do
      { name: 'Fantasy',
        icon: 'fas fa-dragon',
        color: 'purple' }
    end

    it 'returns true for valid record' do
      tag = Tag.new(valid_attrs)

      expect(tag).to be_valid
    end

    it 'returns errors when name is blank' do
      invalid_attrs = valid_attrs.merge(name: '    ')
      tag = Tag.new(invalid_attrs)

      expect(tag).not_to be_valid
      expect(tag).to have_validation_error(:name, 'can\'t be blank')
    end

    it 'returns errors when name already taken' do
      tag = Tag.new(valid_attrs)

      # Create tag with same name
      FactoryBot.create(:tag, name: tag.name)

      expect(tag).not_to be_valid
      expect(tag).to have_validation_error(:name, 'has already been taken')

      # Does not persist record when skipping validations, due to uniqueness index
      expect { tag.save!(validate: false) }.to raise_error(ActiveRecord::RecordNotSaved)
    end
  end
end
