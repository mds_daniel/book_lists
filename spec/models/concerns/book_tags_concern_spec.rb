require 'rails_helper'
require_relative '../../support/validation_error_matcher.rb'

RSpec.describe BookTagsConcern, type: :model do
  let(:book) { FactoryBot.create(:book, title: 'The Name of the Wind') }
  let(:tag_a) { FactoryBot.create(:tag, name: 'Fantasy') }
  let(:tag_b) { FactoryBot.create(:tag, name: 'Travelling') }

  describe 'add_tag' do
    it 'adds tag to book' do
      expect(book.tags).to be_empty

      # Add tag_a
      book_tag = book.add_tag(tag_a)

      expect(book_tag).to have_attributes(book_id: book.id, tag_id: tag_a.id)
      expect(book.tags.reload).to contain_exactly(tag_a)

      # Add tag_b
      book_tag = book.add_tag(tag_b)

      expect(book_tag).to have_attributes(book_id: book.id, tag_id: tag_b.id)
      expect(book.tags.reload).to contain_exactly(tag_a, tag_b)
    end

    it 'allows the same tag to be added multiple times' do
      book.add_tag(tag_a)
      book.add_tag(tag_a)

      expect(book.tags.reload).to contain_exactly(tag_a)
    end

    it 'returns false and sets error when receiving blank tag_id' do
      expect(book.add_tag('    ')).to eq(false)
      expect(book).to have_validation_error(:new_book_tag_id, 'can\'t be blank')
    end

    it 'returns false when receiving invalid tag id' do
      tag_a.destroy!

      expect(book.add_tag(tag_a)).to eq(false)
      expect(book).to have_validation_error(:new_book_tag_id, 'was not found')
    end

    it 'raises error when called on a book that was not yet persisted' do
      new_book = book.dup # will not have `id` set as it is a new record

      expect { new_book.add_tag(tag_a) }.to raise_error('Must only be called on persisted instance')
    end
  end

  describe 'remove_tag' do
    before do
      book.tags = [tag_a, tag_b]
    end

    it 'removes tag from book' do
      expect(book.tags.reload).to contain_exactly(tag_a, tag_b)

      # Remove tag_a
      book_tag = book.remove_tag(tag_a)

      expect(book_tag).to have_attributes(book_id: book.id, tag_id: tag_a.id)
      expect(book.tags.reload).to contain_exactly(tag_b)

      # Remove tag_b
      book_tag = book.remove_tag(tag_b)

      expect(book_tag).to have_attributes(book_id: book.id, tag_id: tag_b.id)
      expect(book.tags.reload).to be_empty
    end

    it 'allows the same tag to be removed multiple times' do
      book_tag_first = book.remove_tag(tag_a)
      book_tag_second = book.remove_tag(tag_a)

      expect(book_tag_first).to have_attributes(book_id: book.id, tag_id: tag_a.id)
      expect(book_tag_second).to be_nil

      expect(book.tags.reload).not_to include(tag_a)
    end
  end
end
