require 'rails_helper'
require_relative '../../support/validation_error_matcher.rb'

RSpec.describe BookAuthorsConcern, type: :model do
  describe 'add_author' do
    let(:book) { FactoryBot.create(:book, title: 'Good Omens') }
    let(:author_a) { FactoryBot.create(:author, name: 'Terry Pratchett') }
    let(:author_b) { FactoryBot.create(:author, name: 'Neil Gaiman') }

    it 'adds author to book' do
      expect(book.authors).to be_empty

      # Add author_a
      author_book = book.add_author(author_a)

      expect(author_book).to have_attributes(book_id: book.id, author_id: author_a.id)
      expect(book.authors.reload).to contain_exactly(author_a)

      # Add author_b
      author_book = book.add_author(author_b)

      expect(author_book).to have_attributes(book_id: book.id, author_id: author_b.id)
      expect(book.authors.reload).to contain_exactly(author_a, author_b)
    end

    it 'allows the same author to be added multiple times' do
      book.add_author(author_a)
      book.add_author(author_a)

      expect(book.authors.reload).to contain_exactly(author_a)
    end

    it 'returns false and sets error when receiving blank author_id' do
      expect(book.add_author('    ')).to eq(false)
      expect(book).to have_validation_error(:new_book_author_id, 'can\'t be blank')
    end

    it 'returns false when receiving invalid id' do
      author_a.destroy!

      expect(book.add_author(author_a)).to eq(false)
      expect(book).to have_validation_error(:new_book_author_id, 'was not found')
    end

    it 'raises error when called on a book that was not yet persisted' do
      new_book = book.dup # will not have `id` set as it is a new record

      expect { new_book.add_author(author_a) }.to raise_error('Must only be called on persisted instance')
    end
  end

  describe 'remove_author' do
    let(:book) { FactoryBot.create(:book, title: 'Good Omens') }
    let(:author_a) { FactoryBot.create(:author, name: 'Terry Pratchett') }
    let(:author_b) { FactoryBot.create(:author, name: 'Neil Gaiman') }

    before do
      book.authors = [author_a, author_b]
    end

    it 'removes author from book' do
      expect(book.authors.reload).to contain_exactly(author_a, author_b)

      # Remove author_a
      author_book = book.remove_author(author_a)

      expect(author_book).to have_attributes(book_id: book.id, author_id: author_a.id)
      expect(book.authors.reload).to contain_exactly(author_b)

      # Remove author_b
      author_book = book.remove_author(author_b)

      expect(author_book).to have_attributes(book_id: book.id, author_id: author_b.id)
      expect(book.authors.reload).to be_empty
    end

    it 'allows the same author to be removed multiple times' do
      author_book_first = book.remove_author(author_a)
      author_book_second = book.remove_author(author_a)

      expect(author_book_first).to have_attributes(book_id: book.id, author_id: author_a.id)
      expect(author_book_second).to be_nil

      expect(book.authors.reload).not_to include(author_a)
    end
  end

  describe 'save_with_new_author' do
    let(:author) { FactoryBot.create(:author) }
    let(:book) do
      Book.new(
        title: 'The Colour of Magic',
        description: 'An awesome fantasy book from the Discworld series.'
      )
    end

    it 'saves book and assigns author' do
      expect(book.save_with_new_author(author.id)).to eq(true)
      expect(book.authors).to contain_exactly(author)
    end

    it 'when author_id is not present saves book ignoring author' do
      expect(book.save_with_new_author(nil)).to eq(true)
      expect(book).to be_persisted
      expect(book.authors).to be_empty
    end

    it 'throws an exception when called on persisted book' do
      book.save!

      expect {
        book.save_with_new_author(author.id)
      }.to raise_error('Must only be called on new instance')
    end

    it 'returns errors when author has been deleted' do
      author.destroy!

      expect(book.save_with_new_author(author.id)).to eq(false)
      expect(book).to have_validation_error(:new_book_author_id, 'was not found')
    end

    it 'returns errors when book attributes are invalid' do
      book.title = '   '

      expect(book.save_with_new_author(author.id)).to eq(false)
      expect(book).to have_validation_error(:title, 'can\'t be blank')
    end
  end
end
