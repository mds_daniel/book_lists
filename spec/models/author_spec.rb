require 'rails_helper'
require_relative '../support/validation_error_matcher.rb'

RSpec.describe Author, type: :model do
  describe 'validation' do
    let(:valid_attrs) do
      {
        name: 'Terry Pratchett',
        bio: 'Sir Terry Pratchett is the author of the Discword fantasy novel series.',
        wiki_url: 'https://en.wikipedia.org/wiki/Terry_Pratchett',
      }
    end

    it 'returns true for valid record' do
      author = Author.new(valid_attrs)

      expect(author).to be_valid
      expect(author).to have_attributes(valid_attrs)
    end

    it 'returns errors when name is blank' do
      invalid_attrs = valid_attrs.merge(name: '   ')
      author = Author.new(invalid_attrs)

      expect(author).not_to be_valid
      expect(author).to have_validation_error(:name, 'can\'t be blank')
    end

    it 'returns errors when name has already been taken' do
      FactoryBot.create(:author, valid_attrs)

      author = Author.new(valid_attrs)

      expect(author).not_to be_valid
      expect(author).to have_validation_error(:name, 'has already been taken')

      expect { author.save!(validate: false) }.to raise_error(ActiveRecord::RecordNotSaved)
    end

    it 'allows bio to be blank' do
      author = Author.new(valid_attrs.merge(bio: '   '))

      expect(author).to be_valid
    end

    it 'returns errors if bio is too short' do
      author = Author.new(valid_attrs.merge(bio: 'short'))

      expect(author).not_to be_valid
      expect(author).to have_validation_error(:bio, 'is too short (minimum is 10 characters)')
    end

    it 'returns errors if wiki_url is not a valid URI' do
      invalid_attrs = valid_attrs.merge(wiki_url: 'this is not a valid URL')
      author = Author.new(invalid_attrs)

      expect(author).not_to be_valid
      expect(author).to have_validation_error(:wiki_url, 'must be a valid URL')
    end

    it 'returns errors if wiki_url does not have English Wikipedia as a host' do
      invalid_attrs = valid_attrs.merge(wiki_url: 'https://google.com')
      author = Author.new(invalid_attrs)

      expect(author).not_to be_valid
      expect(author).to have_validation_error(:wiki_url, 'must be a link to English Wikipedia')
    end

    it 'returns errors if wiki_url does not have https as scheme' do
      invalid_attrs = valid_attrs.merge(wiki_url: 'http://en.wikipedia.org/wiki/Terry_Pratchett')
      author = Author.new(invalid_attrs)

      expect(author).not_to be_valid
      expect(author).to have_validation_error(:wiki_url, 'must have https as URI scheme')
    end
  end
end
