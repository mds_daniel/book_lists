require 'rails_helper'
require_relative '../support/validation_error_matcher.rb'

RSpec.describe BookReview, type: :model do
  let(:book) { FactoryBot.build(:book) }
  let(:user) { FactoryBot.build(:user) }

  describe 'validation' do
    let(:review) do
      BookReview.new(
        book: book,
        user: user,
        body: 'Really interesting book!'
      )
    end

    it 'returns true for a valid record' do
      expect(review).to be_valid
    end

    it 'returns errors when body is blank' do
      review.body = ''

      expect(review).not_to be_valid
      expect(review).to have_validation_error(:body, 'can\'t be blank')
    end

    it 'returns errors when body is too short' do
      review.body = 'short'

      expect(review).not_to be_valid
      expect(review).to have_validation_error(:body, 'is too short (minimum is 10 characters)')
    end

    it 'returns errors when book is missing' do
      review.book = nil

      expect(review).not_to be_valid
      expect(review).to have_validation_error(:book, 'must exist')
    end

    it 'returns errors when user is missing' do
      review.user = nil

      expect(review).not_to be_valid
      expect(review).to have_validation_error(:user, 'must exist')
    end

    it 'returns errors when duplicate review' do
      # create a review for the same user and same book
      FactoryBot.create(:book_review, book: book, user: user)

      expect(review).not_to be_valid
      expect(review).to have_validation_error(:book_id, "has already been reviewed")

      # Validate uniqueness index contraint is turned into error
      expect { review.save!(validate: false) }.to raise_error(ActiveRecord::RecordNotSaved)
    end
  end

  describe 'counter_cache on books' do
    let(:other_user) { FactoryBot.create(:user) }

    it 'returns correct count of book_reviews for each book' do
      book.save!
      expect(book.book_reviews_count).to eq(0)

      # Create first review
      review = book.book_reviews.create(FactoryBot.attributes_for(:book_review, user: user))
      book.reload

      expect(book.book_reviews_count).to eq(1)

      # Create second review
      book.book_reviews.create(FactoryBot.attributes_for(:book_review, user: other_user))
      book.reload

      expect(book.book_reviews_count).to eq(2)

      # Destroy first review
      review.destroy!
      book.reload
      expect(book.book_reviews_count).to eq(1)
    end
  end
end
