require 'rails_helper'

RSpec.describe 'Viewing books', type: :system do
  let!(:book_a) { FactoryBot.create(:book, title: 'Warbreaker', authors: [author_a]) }
  let!(:book_b) { FactoryBot.create(:book, title: 'The Name of the Wind', authors: [author_b]) }

  let(:author_a) { FactoryBot.create(:author, name: 'Brandon Sanderson') }
  let(:author_b) { FactoryBot.create(:author, name: 'Patrick Rothfuss') }

  before do
    visit '/'
    click_link 'Books'
  end

  it 'shows all available books' do
    expect(page).to have_css('.book-title', text: book_a.title)
    expect(page).to have_css('.book-description', text: book_a.description)

    expect(page).to have_css('.book-title', text: book_b.title)
    expect(page).to have_css('.book-description', text: book_b.description)
  end

  it 'allows searching for books' do
    # Search for `book_b` title
    fill_in 'search[title]', with: 'Wind'
    click_button 'Search'

    expect(page).to have_no_css('.book-title', text: book_a.title)
    expect(page).to have_css('.book-title', text: book_b.title)

    # Reset shows all books again
    click_link 'Reset'
    expect(page).to have_css('.book-title', text: book_a.title)
    expect(page).to have_css('.book-title', text: book_b.title)

    # Search for `author_a`
    select author_a.name, from: 'search[author_id]'
    click_button 'Search'

    expect(page).to have_css('.book-title', text: book_a.title)
    expect(page).to have_no_css('.book-title', text: book_b.title)
  end
end
