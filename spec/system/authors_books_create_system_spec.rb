require 'rails_helper'

RSpec.describe 'Adding authors to books', type: :system do
  let(:user) { FactoryBot.create(:user, :staff) }
  let!(:book) { FactoryBot.create(:book, title: 'Good Omens') }

  let!(:author_a) { FactoryBot.create(:author, name: 'Terry Pratchett') }
  let!(:author_b) { FactoryBot.create(:author, name: 'Neil Gaiman') }

  before do
    sign_in user
    visit "/"
    click_link "Books"
    click_link book.title
  end

  context 'when signed in as staff member' do
    it 'allows user to add authors to book' do
      expect(page).to have_no_author_tag(author_a.name)
      expect(page).to have_no_author_tag(author_b.name)

      # Add first author
      click_link 'Add author'
      select author_a.name, from: 'Author'
      click_button 'Save'

      expect(page).to have_text('Author added successfully.')
      expect(page).to have_author_tag(author_a.name)
      expect(page).to have_no_author_tag(author_b.name)

      # Adding second author
      click_link 'Add author'
      select author_b.name, from: 'Author'
      click_button 'Save'

      expect(page).to have_text('Author added successfully.')
      expect(page).to have_author_tag(author_a.name)
      expect(page).to have_author_tag(author_b.name)
    end

    it 'returns errors when form not filled in' do
      click_link 'Add author'

      # Forget to choose author
      click_button 'Save'

      expect(page).to have_help_message('New book author can\'t be blank')
    end
  end

  context 'when signed in as a regular user' do
    let(:user) { FactoryBot.create(:user) }

    it 'does not show add author link' do
      expect(page).to have_no_link('Add author')
    end
  end
end
