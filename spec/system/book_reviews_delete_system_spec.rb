require 'rails_helper'

RSpec.describe 'Deleting book reviews', type: :system do
  let(:user) { FactoryBot.create(:user) }
  let(:book) { FactoryBot.create(:book) }
  let!(:book_review) { FactoryBot.create(:book_review, book: book, user: user) }

  before do
    sign_in user
    visit '/'
    click_link 'Books'
  end

  context 'when viewing own book review' do
    before do
      click_link 'You\'ve reviewed this book'
    end

    it 'allows users to delete their book review' do
      accept_confirm 'Are you sure you want to delete this review?' do
        click_link 'Delete book review'
      end

      expect(page).to have_text('Book review deleted successfully.')
      expect(page).not_to have_text(book_review.body)
    end
  end

  context 'when viewing another user\'s book review' do
    let(:other_user) { FactoryBot.create(:user, email: 'other@example.com') }
    let!(:book_review) { FactoryBot.create(:book_review, book: book, user: other_user) }

    it 'does not show edit link' do
      expect(page).to have_no_link('You\'ve reviewed this book')

      click_link '1 book review'

      expect(page).to have_text(book_review.body)
      expect(page).to have_no_link('Delete book review')
    end
  end
end
