require 'rails_helper'

RSpec.describe 'Deleting books', type: :system do
  let(:user) { FactoryBot.create(:user, :admin) }
  let!(:book) { FactoryBot.create(:book) }

  before do
    sign_in user
    visit "/"
    click_link "Books"
    click_link book.title
  end

  it 'allows admins to delete existing books' do
    accept_confirm 'Are you sure you want to delete this book?' do
      click_link 'Delete book'
    end

    expect(page).to have_text('Book deleted successfully.')
    expect(page).to have_no_css('.book-title', text: book.title)
  end

  context 'when staff member' do
    let(:user) { FactoryBot.create(:user, :staff) }

    it 'hides delete book links' do
      expect(page).to have_no_css('a', text: 'Delete book')
    end
  end
end
