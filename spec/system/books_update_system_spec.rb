require 'rails_helper'

RSpec.describe 'Updating books', type: :system do
  let(:user) { FactoryBot.create(:user, :staff) }
  let!(:book) { FactoryBot.create(:book) }

  let(:updated_book_title) { 'Updated Awesome Book Title' }
  let(:updated_book_description) { 'With updated awesome description' }

  before do
    sign_in user
    visit "/"
    click_link "Books"
    click_link book.title
    click_link "Edit book"
  end

  it 'allows staff members to update existing books' do
    fill_in 'Title', with: updated_book_title
    fill_in 'Description', with: updated_book_description
    click_button 'Save'

    expect(page).to have_text('Book updated successfully.')
    expect(page).to have_css('.book-title', text: updated_book_title)
    expect(page).to have_css('.book-description', text: updated_book_description)
  end

  it 'returns errors when form not filled correctly' do
    fill_in 'Title', with: '' # empty title
    click_button 'Save'

    expect(page).to have_text('Failed to update book!')
    expect(page).to have_help_message('Title can\'t be blank')
  end
end
