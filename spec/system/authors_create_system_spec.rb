require 'rails_helper'

RSpec.describe 'Creating authors', type: :system do
  context 'when signed in as a staff member' do
    let(:user) { FactoryBot.create(:user, :staff) }

    before do
      sign_in user
      visit '/'
      click_link 'Authors'
      click_link 'Add new author'
    end

    it 'allows staff members to create new authors' do
      fill_in 'Name', with: 'Terry Pratchett'
      fill_in 'Wikipedia URL', with: 'https://en.wikipedia.org/Terry_Pratchett'
      fill_in 'Bio', with: 'Awesome fantasy book author'
      click_button 'Save'

      expect(page).to have_text('Author created successfully.')
      expect(page).to have_css('.author-name', text: 'Terry Pratchett')
    end

    it 'returns errors when form is not filled in correctly' do
      # forget to fill in form
      click_button 'Save'

      expect(page).to have_text('Failed to create author!')
      expect(page).to have_help_message('Name can\'t be blank')
    end
  end

  context 'when signed in as a regular user' do
    let(:user) { FactoryBot.create(:user) }

    before do
      sign_in user
      visit '/'
      click_link 'Authors'
    end

    it 'does not show create author link' do
      expect(page).to have_no_link('Add new author')
    end
  end
end
