require 'rails_helper'

RSpec.describe 'Adding tags to books', type: :system do
  let(:user) { FactoryBot.create(:user, :staff) }
  let!(:book) { FactoryBot.create(:book, title: 'The Name of the Wind') }

  let!(:tag_a) { FactoryBot.create(:tag, name: 'Fantasy') }
  let!(:tag_b) { FactoryBot.create(:tag, name: 'Travelling') }

  before do
    sign_in user
    visit "/"
    click_link "Books"
    click_link book.title
  end

  context 'when signed in as a staff member' do
    it 'allows user to add tags to book' do
      expect(page).to have_no_book_tag(tag_a.name)
      expect(page).to have_no_book_tag(tag_b.name)

      # Add first tag
      click_link 'Add tag'
      select tag_a.name, from: 'Tag'
      click_button 'Save'

      expect(page).to have_text('Tag added successfully.')
      expect(page).to have_book_tag(tag_a.name)
      expect(page).to have_no_book_tag(tag_b.name)

      # Add second tag
      click_link 'Add tag'
      select tag_b.name, from: 'Tag'
      click_button 'Save'

      expect(page).to have_text('Tag added successfully')
      expect(page).to have_book_tag(tag_a.name)
      expect(page).to have_book_tag(tag_b.name)
    end

    it 'returns errors when form not filled in' do
      click_link 'Add tag'

      # Forget to choose tag
      click_button 'Save'

      expect(page).to have_help_message('New book tag can\'t be blank')
    end
  end

  context 'when signed in as a regular user' do
    let(:user) { FactoryBot.create(:user) }

    it 'does not show add tag link' do
      expect(page).to have_no_link('Add tag')
    end
  end
end
