require 'rails_helper'

RSpec.describe 'Deleting tags', type: :system do
  let(:user) { FactoryBot.create(:user, :admin) }
  let!(:tag) { FactoryBot.create(:tag) }

  before do
    sign_in user
    visit "/"
    click_link "Tags"
  end

  it 'allows admins to delete existing tags' do
    expect(page).to have_css(tag_row_id(tag))

    accept_confirm 'Are you sure you want to delete this tag?' do
      within tag_row_id(tag)  do
        click_link 'Delete tag'
      end
    end

    expect(page).to have_text('Tag deleted successfully.')
    expect(page).to have_no_css(tag_row_id(tag))
  end

  context 'when logged in as staff' do
    let(:user) { FactoryBot.create(:user, :staff) }

    it 'hides delete tag links' do
      expect(page).to have_no_link('Delete tag')
    end
  end

  def tag_row_id(tag)
    "#tag-row-#{tag.id}"
  end
end
