require 'rails_helper'

RSpec.describe 'Updating authors', type: :system do
  context 'when signed in as a staff member' do
    let(:user) { FactoryBot.create(:user, :staff) }
    let!(:author) { FactoryBot.create(:author, name: 'Brandon Sanderson') }

    before do
      sign_in user
      visit '/'
      click_link 'Authors'
      click_link 'Edit author'
    end

    it 'allows staff members to update existing authors' do
      fill_in 'Name', with: 'Brandon Updated Sanderson'
      fill_in 'Wikipedia URL', with: 'https://en.wikipedia.org/Brandon_Updated_Sanderson'
      fill_in 'Bio', with: 'Another awesome fantasy writer, now updated!'
      click_button 'Save'

      expect(page).to have_text('Author updated successfully.')
      expect(page).to have_css('.author-name', text: 'Brandon Updated Sanderson')
    end
  end

  context 'when signed in as a regular user' do
    let(:user) { FactoryBot.create(:user) }

    before do
      sign_in user
      visit '/'
      click_link 'Authors'
    end

    it 'does not show edit author link' do
      expect(page).to have_no_link('Edit author')
    end
  end
end
