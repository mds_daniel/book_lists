require 'rails_helper'

RSpec.describe 'Creating tags', type: :system do
  context 'when signed in as a staff member' do
    let(:user) { FactoryBot.create(:user, :staff) }

    before do
      sign_in user
      visit '/'
      click_link 'Tags'
      click_link 'Add new tag'
    end

    it 'allows staff members to create new tags' do
      fill_in 'Name', with: 'Fantasy'
      fill_in 'Icon', with: 'fas fa-dragon'
      fill_in 'Color', with: '#663399'
      click_button 'Save'

      expect(page).to have_text('Tag created successfully.')
      expect(page).to have_css('.tag-name', text: 'Fantasy')
    end

    it 'returns errors when form is not filled in correctly' do
      # forget to fill in form
      click_button 'Save'

      expect(page).to have_text('Failed to create tag!')
      expect(page).to have_help_message('Name can\'t be blank')
    end
  end

  context 'when signed in as a regular user' do
    let(:user) { FactoryBot.create(:user) }

    before do
      sign_in user
      visit '/'
      click_link 'Tags'
    end

    it 'does not show create tag link' do
      expect(page).to have_no_link('Add new tag')
    end
  end
end
