require 'rails_helper'

RSpec.describe 'Removing authors from books', type: :system do
  let(:user) { FactoryBot.create(:user, :staff) }
  let(:book) { FactoryBot.create(:book, title: 'Good Omens') }

  let(:author_a) { FactoryBot.create(:author, name: 'Terry Pratchett') }
  let(:author_b) { FactoryBot.create(:author, name: 'Neil Gaiman') }

  before do
    sign_in user
    book.authors = [author_a, author_b]

    visit "/"
    click_link "Books"
    click_link book.title
  end

  context 'when signed in as staff member' do
    it 'allows user to remove authors from book' do
      expect(page).to have_author_tag(author_a.name)
      expect(page).to have_author_tag(author_b.name)

      click_remove_author_link(author_a)

      expect(page).to have_text('Author removed successfully.')
      expect(page).to have_no_author_tag(author_a.name)
      expect(page).to have_author_tag(author_b.name)

      click_remove_author_link(author_b)

      expect(page).to have_text('Author removed successfully.')
      expect(page).to have_no_author_tag(author_a.name)
      expect(page).to have_no_author_tag(author_b.name)
    end
  end

  context 'when signed in as a regular user' do
    let(:user) { FactoryBot.create(:user) }

    it 'does not show remove author links' do
      expect(page).to have_no_link('Remove author')
    end
  end

  def click_remove_author_link(author)
    accept_confirm 'Are you sure you want to remove this author?' do
      within "#book-author-#{author.id}-tag" do
        click_link 'Remove author'
      end
    end
  end
end
