require 'rails_helper'

RSpec.describe 'Creating book reviews', type: :system do
  let(:user) { FactoryBot.create(:user) }
  let!(:book) { FactoryBot.create(:book) }

  before do
    sign_in user
    visit '/'
    click_link 'Books'
    click_link 'Add book review'
  end

  it 'allows users to create new book reviews' do
    fill_in 'book_review[body]', with: 'An awesome book! I really enjoyed reading it.'
    click_button 'Save'

    expect(page).to have_text('Book review created successfully.')
  end

  it 'returns errors when form not filled in' do
    # forget to fill in form
    click_button 'Save'

    expect(page).to have_text('Failed to create book review!')
    expect(page).to have_help_message('Body can\'t be blank')
  end

  it 'returns errors when book has already been reviewed' do
    # Create book review for same user and book before submitting form
    FactoryBot.create(:book_review, user: user, book: book)

    fill_in 'book_review[body]', with: 'An awesome book! I really enjoyed reading it.'

    click_button 'Save'

    expect(page).to have_text('Failed to create book review!')
    expect(page).to have_help_message('Book has already been reviewed')
  end
end
