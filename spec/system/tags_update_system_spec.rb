require 'rails_helper'

RSpec.describe 'Updating tags', type: :system do
  context 'when signed in as a staff member' do
    let(:user) { FactoryBot.create(:user, :staff) }
    let!(:tag) { FactoryBot.create(:tag, name: 'Science Fiction') }

    before do
      sign_in user
      visit '/'
      click_link 'Tags'
      click_link 'Edit tag'
    end

    it 'allows staff members to update existing Tag' do
      fill_in 'Name', with: 'Science Updated Fiction'
      fill_in 'Icon', with: 'fas fa-user-astronaut'
      fill_in 'Color', with: '#663399'
      click_button 'Save'

      expect(page).to have_text('Tag updated successfully.')
      expect(page).to have_tag_label(tag.id, 'Science Updated Fiction')
    end
  end

  context 'when signed in as a regular user' do
    let(:user) { FactoryBot.create(:user) }

    before do
      sign_in user
      visit '/'
      click_link 'Tags'
    end

    it 'does not show edit tag link' do
      expect(page).to have_no_link('Edit tag')
    end
  end

  def have_tag_label(tag_id, label)
    have_css("#book-tag-#{tag_id}", text: label)
  end
end
