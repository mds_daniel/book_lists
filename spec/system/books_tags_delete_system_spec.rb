require 'rails_helper'

RSpec.describe 'Removing tags from books', type: :system do
  let(:user) { FactoryBot.create(:user, :staff) }
  let(:book) { FactoryBot.create(:book, title: 'The Name of the Wind') }

  let(:tag_a) { FactoryBot.create(:tag, name: 'Fantasy') }
  let(:tag_b) { FactoryBot.create(:tag, name: 'Travelling') }

  before do
    sign_in user
    book.tags = [tag_a, tag_b]

    visit "/"
    click_link "Books"
    click_link book.title
  end

  context 'when signed in as staff member' do
    it 'allows user to remove tags from book' do
      expect(page).to have_book_tag(tag_a.name)
      expect(page).to have_book_tag(tag_b.name)

      # Remove first tag
      click_remove_tag_link(tag_a)

      expect(page).to have_text('Tag removed successfully.')
      expect(page).to have_no_book_tag(tag_a.name)
      expect(page).to have_book_tag(tag_b.name)

      # Remove second tag
      click_remove_tag_link(tag_b)

      expect(page).to have_text('Tag removed successfully.')
      expect(page).to have_no_book_tag(tag_a.name)
      expect(page).to have_no_book_tag(tag_b.name)
    end
  end

  context 'when signed in as a regular user' do
    let(:user) { FactoryBot.create(:user) }

    it 'does not show remove tag links' do
      expect(page).to have_no_link('Remove tag')
    end
  end

  def click_remove_tag_link(tag)
    accept_confirm 'Are you sure you want to remove this tag?' do
      within "#book-tag-#{tag.id}" do
        click_link 'Remove tag'
      end
    end
  end
end
