require 'rails_helper'

RSpec.describe 'Signing up to the application', type: :system do
  before do
    visit '/'
    click_link "Sign up"
  end

  it 'allows users to sign up for a new account' do
    fill_in 'Email', with: 'test_user@example.com'
    fill_in 'Password', with: 'Super secret'
    fill_in 'Password confirmation', with: 'Super secret'
    click_button 'Sign up'

    expect(page).to have_text('Welcome! You have signed up successfully.')
  end

  it 'returns errors when users don\'t fill in the form' do
    # Forget to enter email and password
    click_button 'Sign up'

    expect(page).not_to have_text('Welcome! You have signed up successfully.')
    expect(page).to have_text('Email can\'t be blank')
  end
end
