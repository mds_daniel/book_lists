require 'rails_helper'

RSpec.describe 'Deleting authors', type: :system do
  context 'when signed in as an admin' do
    let(:user) { FactoryBot.create(:user, :admin) }
    let!(:author) { FactoryBot.create(:author) }

    before do
      sign_in user
      visit '/'
      click_link 'Authors'
    end

    it 'allows staff members to delete existing authors' do
      # precondition
      expect(page).to have_css('.author-name', text: author.name)

      accept_confirm 'Are you sure you want to delete this author?' do
        click_link 'Delete author'
      end

      expect(page).to have_text('Author deleted successfully.')
      expect(page).to have_current_path(authors_path)
      expect(page).to have_no_css('.author-name', text: author.name)
    end
  end

  context 'when signed in as a staff user' do
    let(:user) { FactoryBot.create(:user, :staff) }

    before do
      sign_in user
      visit '/'
      click_link 'Authors'
    end

    it 'does not show delete author link' do
      expect(page).to have_no_link('Delete author')
    end
  end
end
