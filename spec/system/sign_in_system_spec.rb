require 'rails_helper'

RSpec.describe 'Signing in to the application', type: :system do
  let(:user) { FactoryBot.create(:user) }

  before do
    visit '/'
    click_link 'Sign in'
  end

  it 'allows users to sign in using an existing account' do
    fill_in 'Email', with: user.email
    fill_in 'Password', with: 'supersecret'
    check 'Remember me'
    click_button 'Sign in'

    expect(page).to have_text('Signed in successfully.')
  end

  it 'returns errors when users don\'t fill in the form' do
    # Forget to enter email and password
    click_button 'Sign in'

    expect(page).not_to have_text('Signed in successfully.')
    expect(page).to have_text('Invalid Email or password')
  end
end
