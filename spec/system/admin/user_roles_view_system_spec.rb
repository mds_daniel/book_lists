require 'rails_helper'

RSpec.describe 'Admins viewing user roles' do
  let(:admin) { FactoryBot.create(:user, :admin, email: 'admin@example.com') }

  let!(:user_a) { FactoryBot.create(:user, :staff, email: 'alice@example.com') }
  let!(:user_b) { FactoryBot.create(:user, email: 'bob@example.com') }
  let!(:user_c) { FactoryBot.create(:user, :staff, email: 'carol@example.com') }

  before do
    sign_in admin
    visit '/'

    within '#admin-links' do
      find('.navbar-link').hover
      click_link 'User roles'
    end
  end

  it 'allows admin to see user roles' do
    expect_user_role(user_a.id, 'alice@example.com', 'Staff')
    expect_user_role(user_b.id, 'bob@example.com', 'Visitor')
    expect_user_role(user_c.id, 'carol@example.com', 'Staff')
    expect_user_role(admin.id, 'admin@example.com', 'Admin')
  end

  def expect_user_role(user_id, email, role_label)
    expect(page).to have_css("#user-role-#{user_id}", text: email)
    expect(page).to have_css("#user-role-#{user_id} .user-role-label", text: role_label)
  end
end
