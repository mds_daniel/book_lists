require 'rails_helper'

RSpec.describe 'Admins updating user roles' do
  let(:admin) { FactoryBot.create(:user, :admin) }

  before do
    # ensure user record is saved
    user

    sign_in admin
    visit '/'

    within '#admin-links' do
      find('.navbar-link').hover
      click_link 'User roles'
    end
  end

  context 'when user is visitor' do
    let(:user) { FactoryBot.create(:user) }

    it 'allows admin to upgrade to staff' do
      # precondition
      expect(page).to have_user_role(user, 'Visitor')

      click_button 'Upgrade to Staff'

      expect(page).to have_text('User role updated successfully.')
      expect(page).to have_user_role(user, 'Staff')
    end
  end

  context 'when user is staff' do
    let(:user) { FactoryBot.create(:user, :staff) }

    it 'allows admin to downgrade to visitor' do
      # precondition
      expect(page).to have_user_role(user, 'Staff')

      click_button 'Downgrade to Visitor'

      expect(page).to have_text('User role updated successfully.')
      expect(page).to have_user_role(user, 'Visitor')
    end
  end

  def have_user_role(user, role)
    have_css("#user-role-#{user.id} .user-role-label", text: role)
  end
end
