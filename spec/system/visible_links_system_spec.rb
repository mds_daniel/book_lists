require 'rails_helper'

RSpec.describe 'Visible links', type: :system do
  context 'when not signed in' do
    it 'does not see admin links' do
      visit "/"
      expect(page).to have_no_css('#admin-links', text: 'Admin')
    end

    it 'sees books link' do
      visit "/"
      expect(page).to have_css('.navbar-item', text: 'Books')

      click_link 'Books'
      expect(page).not_to have_css('a', text: 'Add new book')
    end

    it 'sees book page but no links' do
      book = FactoryBot.create(:book)

      visit "/books"
      click_link book.title

      expect(page).to have_css('.book-title', text: book.title)
      expect(page).not_to have_link('Review')
    end

    it 'sees authors link but not page actions' do
      visit "/"
      expect(page).to have_css('.navbar-item', text: 'Authors')

      click_link 'Authors'

      expect(page).not_to have_link('Add new author')
    end
  end

  context 'when visitor' do
    let(:user) { FactoryBot.create(:user) }
    before { sign_in user }

    it 'does not see admin links' do
      visit "/"
      expect(page).to have_no_css('#admin-links', text: 'Admin')
    end

    it 'sees books link' do
      visit "/"
      expect(page).to have_css('.navbar-item', text: 'Books')

      click_link 'Books'
      expect(page).not_to have_css('a', text: 'Add new book')
    end

    it 'sees book page and review link' do
      book = FactoryBot.create(:book)

      visit "/books"
      click_link book.title

      expect(page).to have_css('.book-title', text: book.title)
      expect(page).to have_link('Review')
    end

    it 'sees authors link but not page actions' do
      visit "/"
      expect(page).to have_css('.navbar-item', text: 'Authors')

      click_link 'Authors'

      expect(page).not_to have_link('Add new author')
    end
  end

  context 'when staff member' do
    let(:user) { FactoryBot.create(:user, :staff) }
    before { sign_in user }

    it 'does not see admin links' do
      visit "/"
      expect(page).to have_no_css('#admin-links', text: 'Admin')
    end

    it 'sees books link' do
      visit "/"
      expect(page).to have_css('.navbar-item', text: 'Books')

      click_link 'Books'
      expect(page).to have_css('a', text: 'Add new book')

      click_link 'Add new book'
      expect(page).to have_content('New book')
    end

    it 'sees book page and action links' do
      book = FactoryBot.create(:book)

      visit "/books"
      click_link book.title

      expect(page).to have_css('.book-title', text: book.title)
      expect(page).to have_link('Review')
    end

    it 'sees authors link and page actions' do
      visit "/"
      expect(page).to have_css('.navbar-item', text: 'Authors')

      click_link 'Authors'

      expect(page).to have_link('Add new author')
    end
  end

  context 'when administrator' do
    let(:user) { FactoryBot.create(:user, :admin) }
    before { sign_in user }

    it 'sees admin links' do
      visit "/"
      expect(page).to have_css('#admin-links', text: 'Admin')
    end

    it 'sees books link' do
      visit "/"
      expect(page).to have_css('.navbar-item', text: 'Books')

      click_link 'Books'
      expect(page).to have_css('a', text: 'Add new book')

      click_link 'Add new book'
      expect(page).to have_content('New book')
    end

    it 'sees book page and action links' do
      book = FactoryBot.create(:book)

      visit "/books"
      click_link book.title

      expect(page).to have_css('.book-title', text: book.title)
      expect(page).to have_link('Review')
    end

    it 'sees authors link and page actions' do
      visit "/"
      expect(page).to have_css('.navbar-item', text: 'Authors')

      click_link 'Authors'

      expect(page).to have_link('Add new author')
    end
  end
end
