require 'rails_helper'

RSpec.describe 'Creating books', type: :system do
  let(:user) { FactoryBot.create(:user, :staff) }
  let(:book_title) { 'How to make friends and influence people' }
  let(:book_description) { 'An awesome book about human relationships' }
  let!(:author) { FactoryBot.create(:author, name: 'Dale Carnegie') }

  before do
    sign_in user
    visit "/"
    click_link "Books"
    click_link "Add new book"
  end

  it 'allows users to create new books' do
    fill_in 'Title', with: book_title
    select 'Dale Carnegie', from: 'Author'
    fill_in 'Description', with: book_description
    click_button 'Save'

    expect(page).to have_text('Book created successfully.')
    expect(page).to have_css('.book-title', text: book_title)
    expect(page).to have_css('.book-description', text: book_description)
    expect(page).to have_css('.book-authors', text: 'Dale Carnegie')
  end

  it 'returns errors when form not filled in' do
    # forget to fill in form
    click_button 'Save'

    expect(page).to have_text('Failed to create book!')
    expect(page).to have_help_message('Title can\'t be blank')
  end
end
