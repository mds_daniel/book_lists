require 'rails_helper'

RSpec.describe 'Updating book reviews', type: :system do
  let(:user) { FactoryBot.create(:user) }
  let(:book) { FactoryBot.create(:book) }
  let!(:book_review) { FactoryBot.create(:book_review, book: book, user: user) }

  before do
    sign_in user
    visit '/'
    click_link 'Books'
  end

  context 'when viewing own book review' do
    before do
      click_link 'You\'ve reviewed this book'
      click_link 'Edit book review'
    end

    it 'allows users to edit their book reviews' do
      fill_in 'book_review[body]', with: 'An updated review of this book!'
      click_button 'Save'

      expect(page).to have_text('Book review updated successfully.')
      expect(page).to have_text('An updated review of this book!')
    end

    it 'returns errors when form not filled in correctly' do
      fill_in 'book_review[body]', with: 'short' # body is too short
      click_button 'Save'

      expect(page).to have_text('Failed to update book review!')
      expect(page).to have_css('.help.is-danger', text: 'Body is too short')
      expect(page).to have_help_message('Body is too short')
    end
  end

  context 'when viewing another user\'s book review' do
    let(:other_user) { FactoryBot.create(:user, email: 'other@example.com') }
    let!(:book_review) { FactoryBot.create(:book_review, book: book, user: other_user) }

    it 'does not show edit link' do
      expect(page).to have_no_link('You\'ve reviewed this book')

      click_link '1 book review'

      expect(page).to have_text(book_review.body)
      expect(page).to have_no_link('Edit book review')
    end
  end
end
