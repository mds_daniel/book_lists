require 'rails_helper'

RSpec.describe UserRole do
  let(:visitor_role) do
    UserRole.build(
      user_id: 12,
      email: 'visitor@example.com',
      role: 'visitor'
    )
  end

  let(:staff_role) do
    UserRole.build(
      user_id: 22,
      email: 'staff@example.com',
      role: 'staff'
    )
  end

  let(:admin_role) do
    UserRole.build(
      user_id: 42,
      email: 'admin_user@example.com',
      role: 'staff',
      admin: true
    )
  end

  describe 'build' do
    context 'for visitor' do
      it 'returns a new visitor instance' do
        expect(visitor_role).to have_attributes(
          user_id: 12,
          email: 'visitor@example.com',
          role: 'visitor',
        )

        expect(visitor_role).not_to be_admin
        expect(visitor_role).not_to be_staff
        expect(visitor_role).to be_visitor
      end
    end

    context 'for staff' do
      it 'returns a new staff instance' do
        expect(staff_role).to have_attributes(
          user_id: 22,
          email: 'staff@example.com',
          role: 'staff',
        )

        expect(staff_role).not_to be_admin
        expect(staff_role).to be_staff
        expect(staff_role).not_to be_visitor
      end
    end


    context 'for admin' do
      it 'returns a new admin instance' do
        expect(admin_role).to have_attributes(
          user_id: 42,
          email: 'admin_user@example.com',
          role: 'staff',
        )

        expect(admin_role).to be_admin
        expect(admin_role).to be_staff
        expect(admin_role).not_to be_visitor
      end
    end
  end

  describe 'role_label' do
    it 'returns correct labels' do
      expect(admin_role.role_label).to eq('Admin')
      expect(staff_role.role_label).to eq('Staff')
      expect(visitor_role.role_label).to eq('Visitor')
    end
  end
end
