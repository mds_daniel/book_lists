require 'rails_helper'

RSpec.describe Books::UpsertHelper do
  describe '.normalize_author_id' do
    it 'turns values into id' do
      expect(normalize_id(Author.new(id: 42))).to eq(42)
      expect(normalize_id(42)).to eq(42)
      expect(normalize_id("42")).to eq(42)
    end

    it 'raises errors on invalid inputs' do
      expect { normalize_id("nope") }.to raise_error(ArgumentError)
      expect { normalize_id(nil) }.to raise_error(/Invalid associated record: nil/)
      expect { normalize_id(Book.new) }.to raise_error(/Invalid associated record/)
    end
  end

  def normalize_id(value)
    Books::UpsertHelper.normalize_id(value)
  end
end
