require 'rails_helper'

RSpec.describe 'Admin user role requests', type: :request do
  let(:admin) { FactoryBot.create(:user, :admin) }
  let(:user) { FactoryBot.create(:user, role: 'visitor') }

  describe 'PATCH /admin/user_roles/:user_id' do
    context 'when admin' do
      before do
        sign_in admin
      end

      it 'updates user role' do
        patch "/admin/user_roles/#{user.id}", params: { user_role: { role: 'staff' } }

        expect(flash[:notice]).to eq('User role updated successfully.')

        user.reload
        expect(user).not_to be_visitor
        expect(user).to be_staff
      end
    end

    context 'when non-admin user' do
      let(:staff_user) { FactoryBot.create(:user, :staff) }

      before do
        sign_in staff_user
      end

      it 'returns error and redirects' do
        patch "/admin/user_roles/#{user.id}", params: { user_role: { role: 'staff' } }

        expect(flash[:alert]).to eq('Admin area is restricted!')
        expect(response).to redirect_to(root_path)

        user.reload
        expect(user).to be_visitor
        expect(user).not_to be_staff
      end
    end
  end
end
