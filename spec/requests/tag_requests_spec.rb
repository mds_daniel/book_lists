require 'rails_helper'

RSpec.describe 'Tag requests', type: :request do
  describe 'GET /tags' do
    let!(:tags) { FactoryBot.create_list(:tag, 5) }

    it 'returns all tags' do
      get '/tags'

      expect(response.status).to eq(200)

      tags.each do |tag|
        expect(response.body).to include(html_escape(tag.name))
      end
    end
  end

  describe 'POST /tags' do
    let(:user) { FactoryBot.create(:user, :staff) }
    let(:valid_params) do
      {
        name: 'Fantasy',
        icon: 'fas fa-dragon',
        color: '#663399',
      }
    end

    context 'with valid params' do
      before do
        sign_in user
      end

      it 'creates a new tag' do
        post '/tags', params: { tag: valid_params }

        expect(flash[:notice]).to eq('Tag created successfully.')
        expect(Tag.last).to have_attributes(valid_params)
      end
    end

    context 'with invalid params' do
      before do
        sign_in user
      end

      it 'returns errors' do
        invalid_params = valid_params.merge(name: '   ')
        post '/tags', params: { tag: invalid_params }

        expect(flash[:alert]).to eq('Failed to create tag!')
        expect(response.body).to include(html_escape('Name can\'t be blank'))
        expect(Tag.last).to be_nil
      end
    end

    context 'when tag params not supplied' do
      before do
        sign_in user
      end

      it 'returns errors' do
        post '/tags', params: {}

        expect(flash[:alert]).to eq('Must supply tag params!')
        expect(response).to redirect_to(new_tag_path)
      end
    end

    context 'when signed in as regular user' do
      let(:user) { FactoryBot.create(:user) }

      before do
        sign_in user
      end

      it 'returns error and redirects' do
        post '/tags', params: { tag: valid_params }

        expect(flash[:alert]).to eq('You are not authorized to perform that action.')
        expect(response).to redirect_to(root_path)
      end
    end

    context 'when not signed in' do
      it 'returns error and redirects' do
        post '/tags', params: { tag: valid_params }

        expect(response).to redirect_to(new_user_session_path)
        expect(flash[:alert]).to eq('You need to sign in or sign up before continuing.')
      end
    end
  end

  describe 'PATCH /tags/:id' do
    let(:user) { FactoryBot.create(:user, :staff) }
    let(:tag) { FactoryBot.create(:tag) }

    let(:valid_params) do
      {
        name: 'Updated Fantasy',
        icon: 'fas fa-updated-dragon',
        color: '#663399',
      }
    end

    context 'with valid params' do
      before do
        sign_in user
      end

      it 'creates a new tag' do
        patch "/tags/#{tag.id}", params: { tag: valid_params }

        expect(flash[:notice]).to eq('Tag updated successfully.')
        expect(tag.reload).to have_attributes(valid_params)
      end
    end

    context 'with invalid params' do
      before do
        sign_in user
      end

      it 'returns errors' do
        invalid_params = valid_params.merge(name: '   ')
        patch "/tags/#{tag.id}", params: { tag: invalid_params }

        expect(flash[:alert]).to eq('Failed to update tag!')
        expect(response.body).to include(html_escape('Name can\'t be blank'))
        expect_attributes_not_to_change(tag, %w(name icon color))
      end
    end

    context 'when tag params not supplied' do
      before do
        sign_in user
      end

      it 'returns errors' do
        patch "/tags/#{tag.id}", params: {}

        expect(flash[:alert]).to eq('Must supply tag params!')
        expect(response).to redirect_to(new_tag_path)
      end
    end

    context 'when signed in as regular user' do
      let(:user) { FactoryBot.create(:user) }

      before do
        sign_in user
      end

      it 'returns error and redirects' do
        patch "/tags/#{tag.id}", params: { tag: valid_params }

        expect(flash[:alert]).to eq('You are not authorized to perform that action.')
        expect(response).to redirect_to(root_path)
      end
    end

    context 'when not signed in' do
      it 'returns error and redirects' do
        patch "/tags/#{tag.id}", params: { tag: valid_params }

        expect(response).to redirect_to(new_user_session_path)
        expect(flash[:alert]).to eq('You need to sign in or sign up before continuing.')
      end
    end
  end

  describe 'DELETE /tags/:id' do
    let(:user) { FactoryBot.create(:user, :admin) }
    let(:tag) { FactoryBot.create(:tag) }

    before do
      sign_in user
    end

    it 'deletes tag' do
      delete "/tags/#{tag.id}"

      expect(flash[:notice]).to eq('Tag deleted successfully.')
      expect(response).to redirect_to(tags_path)

      expect { Tag.find(tag.id) }.to raise_error(ActiveRecord::RecordNotFound)
    end

    context 'when tag already deleted' do
      before do
        tag.destroy!
      end

      it 'returns error and redirects' do
      delete "/tags/#{tag.id}"

        expect(flash[:alert]).to eq('Tag not found!')
        expect(response).to redirect_to(tags_path)
      end
    end

    context 'when signed in as a staff user' do
      let(:user) { FactoryBot.create(:user, :staff) }

      it 'returns error and redirects' do
        delete "/tags/#{tag.id}"

        expect(flash[:alert]).to eq('You are not authorized to perform that action.')
        expect(response).to redirect_to(root_path)
      end
    end
  end
end
