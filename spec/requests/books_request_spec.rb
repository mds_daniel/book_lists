require 'rails_helper'

RSpec.describe 'Book requests', type: :request do
  describe 'GET /books' do
    let!(:books) { FactoryBot.create_list(:book, 3) }

    it 'returns all books' do
      get '/books'

      expect(response.status).to eq(200)

      books.each do |book|
        expect(response.body).to include(html_escape(book.title))
        expect(response.body).to include(html_escape(book.description))
      end
    end
  end

  describe 'POST /books' do
    let(:user) { FactoryBot.create(:user, :staff) }
    let(:author) { FactoryBot.create(:author) }

    let(:valid_params) do
      { title: 'Dune',
        description: 'A sci-fi classic' }
    end

    context 'with valid params' do
      before do
        sign_in user
      end

      it 'creates a new book' do
        post '/books', params: { book: valid_params }

        expect(flash[:notice]).to eq('Book created successfully.')
        expect(Book.last).to have_attributes(valid_params)
      end

      it 'creates a new book with author' do
        post '/books', params: { book: valid_params, author_id: author.id }

        expect(flash[:notice]).to eq('Book created successfully.')

        book = Book.last
        expect(book).to have_attributes(valid_params)
        expect(book.authors).to contain_exactly(author)
      end
    end

    context 'with invalid params' do
      before do
        sign_in user
      end

      it 'returns errors' do
        invalid_params = { title: '', description: 'A sci-fi classic' }
        post '/books', params: { book: invalid_params }

        expect(flash[:alert]).to eq('Failed to create book!')
        expect(response.body).to include(html_escape("Title can't be blank"))
        expect(Book.last).to be_nil
      end
    end

    context 'when book params not supplied' do
      before do
        sign_in user
      end

      it 'returns errors' do
        post '/books', params: {}

        expect(flash[:alert]).to eq('Must supply book params!')
        expect(response).to redirect_to(new_book_path)
      end
    end

    context 'when signed in as regular user' do
      let(:user) { FactoryBot.create(:user) }

      before do
        sign_in user
      end

      it 'returns error and redirects' do
        post '/books', params: { book: { title: 'Dune', description: 'A sci-fi classic' } }

        expect(flash[:alert]).to eq('You are not authorized to perform that action.')
        expect(response).to redirect_to(root_path)
      end
    end

    context 'when not signed in' do
      it 'returns error and redirects' do
        post '/books', params: { book: { title: 'Dune', description: 'A sci-fi classic' } }

        expect(response).to redirect_to(new_user_session_path)
        expect(flash[:alert]).to eq('You need to sign in or sign up before continuing.')
      end
    end
  end

  describe 'PATCH /books/:id' do
    let(:user) { FactoryBot.create(:user, :staff) }
    let(:book) { FactoryBot.create(:book) }

    let(:updated_book_title) { 'Updated Awesome Book Title' }
    let(:updated_book_description) { 'With updated awesome description' }
    let(:update_params) do
      { title: updated_book_title,
        description: updated_book_description }
    end

    context 'with valid params' do
      before do
        sign_in user
      end

      it 'updates book' do
        patch "/books/#{book.id}", params: { book: update_params }

        expect(flash[:notice]).to eq('Book updated successfully.')

        expect(Book.find(book.id)).to have_attributes(
          title: updated_book_title,
          description: updated_book_description
        )
      end
    end

    context 'with invalid params' do
      before do
        sign_in user
      end

      it 'returns errors' do
        invalid_params = { title: '' }
        patch "/books/#{book.id}", params: { book: invalid_params }

        expect(flash[:alert]).to eq('Failed to update book!')
        expect(response.body).to include(html_escape("Title can't be blank"))

        # Assert book attributes have not been changed
        expect(Book.find(book.id)).to have_attributes(
          title: book.title,
          description: book.description
        )
      end
    end

    context 'when book params not supplied' do
      before do
        sign_in user
      end

      it 'returns errors' do
        patch "/books/#{book.id}", params: {}

        expect(flash[:alert]).to eq('Must supply book params!')
        expect(response).to redirect_to(edit_book_path(book))
      end
    end

    context 'when signed in as regular user' do
      let(:user) { FactoryBot.create(:user) }

      before do
        sign_in user
      end

      it 'returns error and redirects' do
        patch "/books/#{book.id}", params: { book: update_params }

        expect(flash[:alert]).to eq('You are not authorized to perform that action.')
        expect(response).to redirect_to(root_path)
      end
    end

    context 'when not signed in' do
      it 'returns error and redirects' do
        patch "/books/#{book.id}", params: { book: update_params }

        expect(response).to redirect_to(new_user_session_path)
        expect(flash[:alert]).to eq('You need to sign in or sign up before continuing.')
      end
    end
  end

  describe 'DELETE /books/:id' do
    let(:user) { FactoryBot.create(:user, :admin) }
    let(:book) { FactoryBot.create(:book) }

    before do
      sign_in user
    end

    it 'deletes book' do
      delete "/books/#{book.id}"

      expect(flash[:notice]).to eq('Book deleted successfully.')
      expect(response).to redirect_to(books_path)

      expect { Book.find(book.id) }.to raise_error(ActiveRecord::RecordNotFound)
    end

    context 'when book already deleted' do
      before do
        book.destroy!
      end

      it 'returns error and redirects' do
        delete "/books/#{book.id}"

        expect(flash[:alert]).to eq('Book not found!')
        expect(response).to redirect_to(books_path)
      end
    end

    context 'when signed in as a staff user' do
      let(:user) { FactoryBot.create(:user, :staff) }

      it 'returns error and redirects' do
        delete "/books/#{book.id}"

        expect(flash[:alert]).to eq('You are not authorized to perform that action.')
        expect(response).to redirect_to(root_path)
      end
    end
  end
end
