require 'rails_helper'

RSpec.describe 'Book review requests', type: :request do
  describe 'GET /books/:book_id/book_reviews' do
    let(:book) { FactoryBot.create(:book) }
    let(:other_book) { FactoryBot.create(:book) }

    let(:user) { FactoryBot.create(:user, email: 'user@example.com') }
    let(:other_user) { FactoryBot.create(:user, email: 'other@example.com') }

    let!(:user_review) { FactoryBot.create(:book_review, book: book, user: user) }
    let!(:other_review) { FactoryBot.create(:book_review, book: book, user: other_user) }
    let!(:different_book_review) { FactoryBot.create(:book_review, book: other_book, user: other_user) }

    it 'returns all reviews for book' do
      get "/books/#{book.id}/book_reviews"

      expect(response.status).to eq(200)

      expect(response.body).to include(book_review_id(user_review))
      expect(response.body).to include(book_review_id(other_review))
      expect(response.body).not_to include(book_review_id(different_book_review))
    end
  end

  describe 'POST /books/:book_id/book_reviews' do
    let(:book) { FactoryBot.create(:book) }
    let(:user) { FactoryBot.create(:user) }
    let(:valid_params) do
     { body: 'An interesting read!' }
    end

    context 'with valid params' do
      before do
        sign_in user
      end

      it 'creates a new book review' do
        post "/books/#{book.id}/book_reviews", params: { book_review: valid_params }

        expect(flash[:notice]).to eq('Book review created successfully.')
        expect(book.book_reviews.last).to have_attributes(
          user_id: user.id,
          body: valid_params[:body]
        )
      end
    end

    context 'with invalid params' do
      before do
        sign_in user
      end

      it 'returns errors' do
        invalid_params = { body: '' }
        post "/books/#{book.id}/book_reviews", params: { book_review: invalid_params }

        expect(flash[:alert]).to eq('Failed to create book review!')
        expect(response.body).to include(html_escape("Body can't be blank"))
        expect(book.book_reviews).to be_empty
      end
    end

    context 'when book review params not supplied' do
      before do
        sign_in user
      end

      it 'returns errors' do
        post "/books/#{book.id}/book_reviews", params: {}

        expect(flash[:alert]).to eq('Must supply book review params!')
        expect(response).to redirect_to(new_book_review_path(book))
      end
    end

    context 'when not signed in' do
      it 'returns error and redirects' do
        post "/books/#{book.id}/book_reviews", params: { book_review: valid_params }

        expect(response).to redirect_to(new_user_session_path)
        expect(flash[:alert]).to eq('You need to sign in or sign up before continuing.')
      end
    end
  end

  describe 'PATCH /books/:book_id/book_reviews' do
    let(:book) { FactoryBot.create(:book) }
    let(:user) { FactoryBot.create(:user, email: 'user@example.com') }
    let(:book_review) do
      FactoryBot.create(:book_review, book: book, user: user, body: 'An excellent book!')
    end

    let(:updated_body) { 'An updated, excellent book about life in general.' }

    context 'with valid params' do
      before do
        sign_in user
      end

      it 'updates an existing review' do
        patch "/books/#{book.id}/book_reviews/#{book_review.id}",
          params: { book_review: { body: updated_body } }

        expect(flash[:notice]).to eq('Book review updated successfully.')
        expect(book_review.reload).to have_attributes(body: updated_body)
      end
    end

    context 'with invalid params' do
      before do
        sign_in user
      end

      it 'returns errors' do
        patch "/books/#{book.id}/book_reviews/#{book_review.id}",
          params: { book_review: { body: 'short' } } # body is too short

        expect(flash[:alert]).to eq('Failed to update book review!')
        expect(response.body).to include(html_escape('Body is too short'))
        expect(book_review.reload).not_to have_attributes(body: updated_body)
      end
    end

    context 'when book review params not supplied' do
      before do
        sign_in user
      end

      it 'returns errors' do
        patch "/books/#{book.id}/book_reviews/#{book_review.id}", params: {}

        expect(flash[:alert]).to eq('Must supply book review params!')
        expect(response).to redirect_to(edit_book_review_path(book, book_review))
      end
    end

    context 'when trying to edit another users review' do
      let(:other_user) { FactoryBot.create(:user, email: 'other@example.com') }
      let(:book_review) do
        FactoryBot.create(:book_review, user: other_user, book: book)
      end

      before do
        sign_in user
      end

      it 'returns error and redirects' do
        patch "/books/#{book.id}/book_reviews/#{book_review.id}",
          params: { book_review: { body: updated_body } }

        expect(flash[:alert]).to eq('You are not authorized to perform that action.')
        expect(response).to redirect_to(root_path)
      end
    end

    context 'when not signed in' do
      it 'returns error and redirects' do
        patch "/books/#{book.id}/book_reviews/#{book_review.id}",
          params: { book_review: { body: updated_body } }

        expect(response).to redirect_to(new_user_session_path)
        expect(flash[:alert]).to eq('You need to sign in or sign up before continuing.')
      end
    end
  end

  describe 'DELETE /books/:book_id/book_reviews/:id' do
    let(:user) { FactoryBot.create(:user, email: 'user@example.com') }
    let(:book) { FactoryBot.create(:book) }
    let(:book_review) { FactoryBot.create(:book_review, book: book, user: user) }

    before do
      sign_in user
    end

    it 'deletes book review' do
      delete "/books/#{book.id}/book_reviews/#{book_review.id}"

      expect(flash[:notice]).to eq('Book review deleted successfully.')
      expect(response).to redirect_to(book_reviews_path(book))

      expect { BookReview.find(book_review.id) }.to raise_error(ActiveRecord::RecordNotFound)
    end

    context 'when trying to delete another user\'s review' do
      let(:other_user) { FactoryBot.create(:user, email: 'other@example.com') }
      let(:book_review) { FactoryBot.create(:book_review, book: book, user: other_user) }

      it 'returns error and redirects' do
        delete "/books/#{book.id}/book_reviews/#{book_review.id}"

        expect(flash[:alert]).to eq('You are not authorized to perform that action.')
        expect(response).to redirect_to(root_path)
      end
    end

    context 'when book review already deleted' do
      before do
        book_review.destroy!
      end

      it 'returns error and redirects' do
        delete "/books/#{book.id}/book_reviews/#{book_review.id}"

        expect(flash[:alert]).to eq('Book review not found!')
        expect(response).to redirect_to(book_reviews_path(book))
      end
    end
  end

  def book_review_id(review)
    "id=\"book-review-#{review.id}\""
  end
end
