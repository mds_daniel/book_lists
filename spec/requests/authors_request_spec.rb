require 'rails_helper'

RSpec.describe 'Author requests', type: :request do
  describe 'GET /authors' do
    let!(:authors) { FactoryBot.create_list(:author, 3) }

    it 'returns all authors' do
      get '/authors'

      expect(response.status).to eq(200)

      authors.each do |author|
        expect(response.body).to include(html_escape(author.name))
      end
    end
  end

  describe 'POST /authors' do
    let(:user) { FactoryBot.create(:user, :staff) }
    let(:valid_params) do
      {
        name: 'Terry Pratchett',
        bio: 'Sir Terry Pratchett is the author of the Discworld fantasy series.',
        wiki_url: 'https://en.wikipedia.org/wiki/Terry_Pratchett',
      }
    end

    context 'with valid params' do
      before do
        sign_in user
      end

      it 'creates a new author' do
        post '/authors', params: { author: valid_params }

        expect(flash[:notice]).to eq('Author created successfully.')
        expect(Author.last).to have_attributes(valid_params)
      end
    end

    context 'with invalid params' do
      before do
        sign_in user
      end

      it 'returns errors' do
        invalid_params = valid_params.merge(name: '   ')
        post '/authors', params: { author: invalid_params }

        expect(flash[:alert]).to eq('Failed to create author!')
        expect(response.body).to include(html_escape('Name can\'t be blank'))
        expect(Author.last).to be_nil
      end
    end

    context 'when author params not supplied' do
      before do
        sign_in user
      end

      it 'returns errors' do
        post '/authors', params: {}

        expect(flash[:alert]).to eq('Must supply author params!')
        expect(response).to redirect_to(new_author_path)
      end
    end

    context 'when signed in as regular user' do
      let(:user) { FactoryBot.create(:user) }

      before do
        sign_in user
      end

      it 'returns error and redirects' do
        post '/authors', params: { author: valid_params }

        expect(flash[:alert]).to eq('You are not authorized to perform that action.')
        expect(response).to redirect_to(root_path)
      end
    end

    context 'when not signed in' do
      it 'returns error and redirects' do
        post '/authors', params: { author: valid_params }

        expect(response).to redirect_to(new_user_session_path)
        expect(flash[:alert]).to eq('You need to sign in or sign up before continuing.')
      end
    end
  end

  describe 'PATCH /authors/:id' do
    let(:user) { FactoryBot.create(:user, :staff) }
    let(:author) { FactoryBot.create(:author) }

    let(:valid_params) do
      {
        name: 'Terry Updated Pratchett',
        bio: 'Sir Terry Updated Pratchett is the author of the Discworld fantasy series.',
        wiki_url: 'https://en.wikipedia.org/wiki/Terry_Updated_Pratchett',
      }
    end

    context 'with valid params' do
      before do
        sign_in user
      end

      it 'updates an existing author' do
        patch "/authors/#{author.id}", params: { author: valid_params }

        expect(flash[:notice]).to eq('Author updated successfully.')
        expect(Author.find(author.id)).to have_attributes(valid_params)
      end
    end

    context 'with invalid params' do
      before do
        sign_in user
      end

      it 'returns errors' do
        invalid_params = valid_params.merge(name: '   ')
        patch "/authors/#{author.id}", params: { author: invalid_params }

        expect(flash[:alert]).to eq('Failed to update author!')
        expect(response.body).to include(html_escape('Name can\'t be blank'))
        expect(Author.find(author.id)).to have_attributes(author.attributes)
      end
    end

    context 'when author params not supplied' do
      before do
        sign_in user
      end

      it 'returns errors' do
        patch "/authors/#{author.id}", params: {}

        expect(flash[:alert]).to eq('Must supply author params!')
        expect(response).to redirect_to(edit_author_path(author))
      end
    end

    context 'when signed in as regular user' do
      let(:user) { FactoryBot.create(:user) }

      before do
        sign_in user
      end

      it 'returns error and redirects' do
        patch "/authors/#{author.id}", params: { author: valid_params }

        expect(flash[:alert]).to eq('You are not authorized to perform that action.')
        expect(response).to redirect_to(root_path)
      end
    end

    context 'when not signed in' do
      it 'returns error and redirects' do
        patch "/authors/#{author.id}", params: { author: valid_params }

        expect(response).to redirect_to(new_user_session_path)
        expect(flash[:alert]).to eq('You need to sign in or sign up before continuing.')
      end
    end
  end

  describe 'DELETE /authors/:id' do
    let(:user) { FactoryBot.create(:user, :admin) }
    let(:author) { FactoryBot.create(:author) }

    before do
      sign_in user
    end

    it 'deletes author' do
      delete "/authors/#{author.id}"

      expect(flash[:notice]).to eq('Author deleted successfully.')
      expect(response).to redirect_to(authors_path)

      expect { Author.find(author.id) }.to raise_error(ActiveRecord::RecordNotFound)
    end

    context 'when author already deleted' do
      before do
        author.destroy!
      end

      it 'returns error and redirects' do
        delete "/authors/#{author.id}"

        expect(flash[:alert]).to eq('Author not found!')
        expect(response).to redirect_to(authors_path)
      end
    end

    context 'when signed in as a staff user' do
      let(:user) { FactoryBot.create(:user, :staff) }

      it 'returns error and redirects' do
        delete "/authors/#{author.id}"

        expect(flash[:alert]).to eq('You are not authorized to perform that action.')
        expect(response).to redirect_to(root_path)
      end
    end
  end
end
