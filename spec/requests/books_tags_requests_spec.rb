require 'rails_helper'

RSpec.describe 'Book tag requests', type: :request do
  describe 'POST /books/:book_id/books_tags' do
    let(:user) { FactoryBot.create(:user, :staff) }

    let(:book) { FactoryBot.create(:book) }
    let(:tag) { FactoryBot.create(:tag) }

    context 'with valid params' do
      before do
        sign_in user
      end

      it 'adds tag to book' do
        post "/books/#{book.id}/books_tags", params: { tag_id: tag.id }

        expect(flash[:notice]).to eq('Tag added successfully.')
        expect(book.tags.reload).to contain_exactly(tag)
      end
    end

    context 'with invalid params' do
      before do
        sign_in user
      end

      it 'returns errors when tag_id invalid' do
        post "/books/#{book.id}/books_tags", params: { tag_id: 0 }

        expect(flash[:alert]).to eq('Failed to add tag!')
        expect(response.body).to include(html_escape('New book tag was not found'))
        expect(book.tags.reload).to be_empty
      end

      it 'returns errors when tag was deleted before request' do
        tag.destroy!

        post "/books/#{book.id}/books_tags", params: { tag_id: tag.id }

        expect(flash[:alert]).to eq('Failed to add tag!')
        expect(response.body).to include(html_escape('New book tag was not found'))
        expect(book.tags.reload).to be_empty
      end

      it 'returns errors when tag_id is missing' do
        post "/books/#{book.id}/books_tags"

        expect(flash[:alert]).to eq('Failed to add tag!')
        expect(response.body).to include(html_escape('New book tag can\'t be blank'))
        expect(book.tags.reload).to be_empty
      end
    end

    context 'when signed in as a regular user' do
      let(:user) { FactoryBot.create(:user) }

      before do
        sign_in user
      end

      it 'returns error and redirects' do
        post "/books/#{book.id}/books_tags", params: { tag_id: tag.id }

        expect(flash[:alert]).to eq('You are not authorized to perform that action.')
        expect(response).to redirect_to(root_path)
      end
    end

    context 'when not signed in' do
      it 'returns error and redirects' do
        post "/books/#{book.id}/books_tags", params: { tag_id: tag.id }

        expect(response).to redirect_to(new_user_session_path)
        expect(flash[:alert]).to eq('You need to sign in or sign up before continuing.')
      end
    end
  end

  describe 'DELETE /books/:book_id/books_tags/:id' do
    let(:user) { FactoryBot.create(:user, :staff) }

    let(:book) { FactoryBot.create(:book) }
    let(:tag) { FactoryBot.create(:tag) }

    before do
      book.tags << tag
    end

    context 'with valid params' do
      before do
        sign_in user
      end

      it 'adds tag to book' do
        delete "/books/#{book.id}/books_tags/#{tag.id}"

        expect(flash[:notice]).to eq('Tag removed successfully.')
        expect(book.tags.reload).to be_empty
      end
    end

    context 'with invalid params' do
      before do
        sign_in user
      end

      it 'returns errors when tag_id invalid' do
        delete "/books/#{book.id}/books_tags/0"

        expect(flash[:alert]).to eq('Failed to remove tag!')
        expect(book.tags.reload).to contain_exactly(tag)
      end

      it 'returns errors when tag was deleted before the request' do
        tag.destroy!

        delete "/books/#{book.id}/books_tags/#{tag.id}"

        expect(flash[:alert]).to eq('Failed to remove tag!')
        expect(book.tags.reload).to be_empty
      end
    end

    context 'when signed in as a regular user' do
      let(:user) { FactoryBot.create(:user) }

      before do
        sign_in user
      end

      it 'returns error and redirects' do
        delete "/books/#{book.id}/books_tags/#{tag.id}"

        expect(flash[:alert]).to eq('You are not authorized to perform that action.')
        expect(response).to redirect_to(root_path)
      end
    end

    context 'when not signed in' do
      it 'returns error and redirects' do
        delete "/books/#{book.id}/books_tags/#{tag.id}"

        expect(response).to redirect_to(new_user_session_path)
        expect(flash[:alert]).to eq('You need to sign in or sign up before continuing.')
      end
    end
  end
end
