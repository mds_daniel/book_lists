require 'rails_helper'

RSpec.describe 'Author book requests', type: :request do
  describe 'POST /books/:book_id/authors_books' do
    let(:user) { FactoryBot.create(:user, :staff) }

    let(:book) { FactoryBot.create(:book) }
    let(:author) { FactoryBot.create(:author) }

    context 'with valid params' do
      before do
        sign_in user
      end

      it 'adds author to book' do
        post "/books/#{book.id}/authors_books", params: { author_id: author.id }

        expect(flash[:notice]).to eq('Author added successfully.')
        expect(book.authors.reload).to contain_exactly(author)
      end
    end

    context 'with invalid params' do
      before do
        sign_in user
      end

      it 'returns errors when author_id invalid' do
        post "/books/#{book.id}/authors_books", params: { author_id: 0 }

        expect(flash[:alert]).to eq('Failed to add author!')
        expect(response.body).to include(html_escape('New book author was not found'))
        expect(book.authors.reload).to be_empty
      end

      it 'returns errors when author was deleted before request' do
        author.destroy!

        post "/books/#{book.id}/authors_books", params: { author_id: author.id }

        expect(flash[:alert]).to eq('Failed to add author!')
        expect(response.body).to include(html_escape('New book author was not found'))
        expect(book.authors.reload).to be_empty
      end

      it 'returns errors when author_id is missing' do
        post "/books/#{book.id}/authors_books"

        expect(flash[:alert]).to eq('Failed to add author!')
        expect(response.body).to include(html_escape('New book author can\'t be blank'))
        expect(book.authors.reload).to be_empty
      end
    end

    context 'when signed in as a regular user' do
      let(:user) { FactoryBot.create(:user) }

      before do
        sign_in user
      end

      it 'returns error and redirects' do
        post "/books/#{book.id}/authors_books", params: { author_id: author.id }

        expect(flash[:alert]).to eq('You are not authorized to perform that action.')
        expect(response).to redirect_to(root_path)
      end
    end

    context 'when not signed in' do
      it 'returns error and redirects' do
        post "/books/#{book.id}/authors_books", params: { author_id: author.id }

        expect(response).to redirect_to(new_user_session_path)
        expect(flash[:alert]).to eq('You need to sign in or sign up before continuing.')
      end
    end
  end

  describe 'DELETE /books/:book_id/authors_books/:id' do
    let(:user) { FactoryBot.create(:user, :staff) }

    let(:book) { FactoryBot.create(:book) }
    let(:author) { FactoryBot.create(:author) }

    before do
      book.authors << author
    end

    context 'with valid params' do
      before do
        sign_in user
      end

      it 'adds author to book' do
        delete "/books/#{book.id}/authors_books/#{author.id}"

        expect(flash[:notice]).to eq('Author removed successfully.')
        expect(book.authors.reload).to be_empty
      end
    end

    context 'with invalid params' do
      before do
        sign_in user
      end

      it 'returns errors when author_id invalid' do
        delete "/books/#{book.id}/authors_books/0"

        expect(flash[:alert]).to eq('Failed to remove author!')
        expect(book.authors.reload).to contain_exactly(author)
      end

      it 'returns errors when author was deleted before the request' do
        author.destroy!

        delete "/books/#{book.id}/authors_books/#{author.id}"

        expect(flash[:alert]).to eq('Failed to remove author!')
        expect(book.authors.reload).to be_empty
      end
    end

    context 'when signed in as a regular user' do
      let(:user) { FactoryBot.create(:user) }

      before do
        sign_in user
      end

      it 'returns error and redirects' do
        delete "/books/#{book.id}/authors_books/#{author.id}"

        expect(flash[:alert]).to eq('You are not authorized to perform that action.')
        expect(response).to redirect_to(root_path)
      end
    end

    context 'when not signed in' do
      it 'returns error and redirects' do
        delete "/books/#{book.id}/authors_books/#{author.id}"

        expect(response).to redirect_to(new_user_session_path)
        expect(flash[:alert]).to eq('You need to sign in or sign up before continuing.')
      end
    end
  end
end
