# frozen_string_literal: true

module SystemSpecHelpers

  def have_help_message(text)
    have_css('.help.is-danger', text: text)
  end

  AUTHOR_TAG_CSS = '.book-authors .tag'

  def have_author_tag(author_name)
    have_css(AUTHOR_TAG_CSS, text: author_name)
  end

  def have_no_author_tag(author_name)
    have_no_css(AUTHOR_TAG_CSS, text: author_name)
  end

  BOOK_TAG_CSS = '.book-tags .tag'

  def have_book_tag(tag_name)
    have_css(BOOK_TAG_CSS, text: tag_name)
  end

  def have_no_book_tag(tag_name)
    have_no_css(BOOK_TAG_CSS, text: tag_name)
  end
end
