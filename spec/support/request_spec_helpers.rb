module RequestSpecHelpers
  def html_escape(text)
    ERB::Util.html_escape(text)
  end

  def expect_attributes_not_to_change(record, keys)
    keys = keys.map(&:to_s)
    attrs = record.attributes.slice(*keys)

    expect(attrs).not_to be_empty
    expect(record.reload).to have_attributes(attrs)
  end
end
