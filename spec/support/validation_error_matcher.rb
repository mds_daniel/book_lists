require 'rspec/expectations'

RSpec::Matchers.define :have_validation_error do |error_key, error_message|
  match do |record|
    record.errors.messages_for(error_key).include?(error_message)
  end

  failure_message do |record|
    "expected record #{record.inspect} to have error #{error_key.inspect} #{error_message.inspect},\n" \
      "  but existing errors are #{record.errors.messages}"
  end

  failure_message_when_negated do |record|
    "expected record #{record.inspect} not to have error #{error_key.inspect} #{error_message.inspect},\n" \
      "  but existing errors are #{record.errors.messages}"
  end
end
