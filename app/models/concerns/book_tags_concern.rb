module BookTagsConcern
  extend ActiveSupport::Concern
  # Very similar to BookAuthorsConcern, but handling tags instead of authors

  # Returns an instance of `Books::BooksTag` when successful.
  # Returns `false` and sets `errors[:new_book_tag_id]` on failure.s
  #
  # This operation uses an upsert statement within a subtransaction.
  #
  # If embedding this operation into a parent transaction, make sure to
  # issue a rollback if you want to rollback the parent transaction:
  #   # within parent transaction
  #     raise ActiveRecord::Rollback unless book.add_tag(tag)
  #
  def add_tag(tag)
    raise 'Must only be called on persisted instance' unless persisted?

    if tag.blank?
      add_new_book_tag_id_error('can\'t be blank')
      return false
    end

    book_tag = Books::BooksTag.add_tag(id, tag)

    unless book_tag
      add_new_book_tag_id_error('was not found')
      return false
    end

    book_tag
  end

  # Returns an instance of `Books::BooksTag` when successful.
  # Returns `nil` when `books_tags` record did not exist in table.
  def remove_tag(tag)
    Books::BooksTag.remove_tag(id, tag)
  end

  private

  def add_new_book_tag_id_error(message)
    errors.add(:new_book_tag_id, message)
  end
end
