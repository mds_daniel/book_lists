module BookAuthorsConcern
  extend ActiveSupport::Concern

  # Returns an instance of `Books::AuthorsBook` when successful.
  # Returns `false` and sets `errors[:new_book_author_id]` on failure.s
  #
  # This operation uses an upsert statement within a subtransaction.
  #
  # If embedding this operation into a parent transaction, make sure to
  # issue a rollback if you want to rollback the parent transaction:
  #   # within parent transaction
  #     raise ActiveRecord::Rollback unless book.add_author(author)
  #
  def add_author(author)
    raise 'Must only be called on persisted instance' unless persisted?

    if author.blank?
      add_new_book_author_id_error('can\'t be blank')
      return false
    end

    author_book = Books::AuthorsBook.add_author(id, author)

    unless author_book
      add_new_book_author_id_error('was not found')
      return false
    end

    author_book
  end

  # Returns an instance of `Books::AuthorsBook` when successful.
  # Returns `nil` when `authors_books` record did not exist in table.
  def remove_author(author)
    Books::AuthorsBook.remove_author(id, author)
  end

  # Allow users to select an author when first submitting a book form
  # Gracefully handle missing author exceptions by adding form error.
  def save_with_new_author(author_id)
    raise 'Must only be called on new instance' unless new_record?

    if author_id.present?
      authors << Author.find(author_id)
    end

    save

  rescue ActiveRecord::RecordNotFound
    add_new_book_author_id_error('was not found')
    false
  end

  private

  def add_new_book_author_id_error(message)
    errors.add(:new_book_author_id, message)
  end
end
