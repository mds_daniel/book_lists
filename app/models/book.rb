class Book < ApplicationRecord
  include PgSearch::Model
  include BookAuthorsConcern
  include BookTagsConcern

  ## Validations
  validates :title, presence: true
  validates :description, presence: true, length: { minimum: 10 }

  ## Relationships
  has_many :book_reviews
  has_and_belongs_to_many :authors
  has_and_belongs_to_many :tags

  ## Scopes
  scope :search_for_index, -> (search_params = {}) do
    Books::Search.new(all.order(:title)).call(search_params)
  end

  pg_search_scope :fulltext_search, against: :title,
    using: { tsearch: { tsvector_column: 'tsv', dictionary: 'english' } }

  ## Instance methods
  def reviewed_by?(user)
    book_reviews.exists?(user_id: user)
  end
end
