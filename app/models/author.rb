class Author < ApplicationRecord
  ## Validations
  validates :name, presence: true
  validates_uniqueness_of :name, rescue_from_duplicate: true
  validates :bio, length: { minimum: 10 }, allow_blank: true
  validates :wiki_url,  wikipedia_url: true

  ## Relationships
  has_and_belongs_to_many :books

  ## Scopes
  scope :for_index, ->() { order(:name) }
  scope :only_id_and_name, ->() { all.select(:id, :name).order(:name) }
  scope :excluding_for_book, ->(book) { where.not(id: book.author_ids) }
end
