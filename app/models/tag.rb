class Tag < ApplicationRecord
  ## Validations
  validates :name, presence: true
  validates_uniqueness_of :name, rescue_from_duplicate: true

  ## Relationships
  has_and_belongs_to_many :books

  ## Scopes
  scope :for_index, ->() { order(:name) }
end
