# frozen_string_literal: true

class User < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable, :trackable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :validatable

  ## Relationships
  has_many :book_reviews

  enum role: {visitor: 'visitor', staff: 'staff'}

  before_save :ensure_admin_has_staff_role

  def self.user_roles
    all.order(:email)
      .pluck(:id, :email, :role, :admin)
      .map do |(id, email, role, admin)|
        UserRole.build(user_id: id, email: email, role: role, admin: admin)
      end
  end

  private

  def ensure_admin_has_staff_role
    self.role = 'staff' if admin?
  end
end
