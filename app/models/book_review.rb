class BookReview < ApplicationRecord
  ## Validations
  validates :body, presence: true, length: { minimum: 10 }
  validates_uniqueness_of :book_id, scope: :user_id,
    rescue_from_duplicate: true, message: 'has already been reviewed'

  ## Relationships
  belongs_to :book, counter_cache: true
  belongs_to :user

  ## Scopes
  scope :for_index, -> () { order(created_at: :desc) }

  scope :for_book_ids, -> (book_ids) { where(book_id: book_ids) }

  scope :for_user, -> (current_user) do
    if current_user
      where(user_id: current_user.id)
    else
      none
    end
  end
end
