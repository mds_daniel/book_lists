# frozen_string_literal: true

class UserRole < Struct.new(:user_id, :email, :role, :admin?)

  def self.build(user_id:, email:, role:, admin: false)
    new(user_id, email, role, admin)
  end

  %w(visitor staff).each do |role_name|
    define_method("#{role_name}?") do
      role_name == role
    end
  end

  def role_label
    return 'Admin' if admin?
    role.capitalize
  end
end
