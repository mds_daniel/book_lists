module Books
  class Search
    def initialize(relation)
      @relation = relation
    end

    def call(search_params = {})
      relation = @relation
      search_params = normalize_params(search_params)

      # Apply search filters
      relation = search_by_title(relation, search_params[:title])
      relation = search_by_authors(relation, search_params[:author_ids])
      relation = search_by_tags(relation, search_params[:tag_ids])

      relation
    end

    private

    def search_by_title(relation, title)
      return relation unless title.present?

      relation.fulltext_search(title)
    end

    def search_by_authors(relation, author_ids)
      return relation unless author_ids.present?

      relation.includes(:authors).where(authors: { id: author_ids })
    end

    def search_by_tags(relation, tag_ids)
      return relation unless tag_ids.present?

      relation.includes(:tags).where(tags: { id: tag_ids })
    end

    def normalize_params(opts)
      # Allow author_ids to be sent through both `author_ids` and `author_id` keys
      # This is useful in forms were we only allow lookup by one author
      opts[:author_ids] = wrap_array(opts[:author_ids]) + wrap_array(opts[:author_id])

      # Same as for `author_ids` above
      opts[:tag_ids] = wrap_array(opts[:tag_ids]) + wrap_array(opts[:tag_id])


      opts
    end

    def wrap_array(value)
      Array.wrap(value.presence)
    end
  end
end

