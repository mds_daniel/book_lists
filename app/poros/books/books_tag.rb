module Books
  class BooksTag < ApplicationRecord
    # Only used for upserting new join table records
    FIELDS = %i(book_id tag_id).freeze

    ## Relationships
    belongs_to :book
    belongs_to :tag

    class << self
      def add_tag(book_id, tag)
        raise 'Book id can\'t be blank' unless book_id.present?

        tag_id = UpsertHelper.normalize_id(tag)
        assoc_ids = { book_id: book_id, tag_id: tag_id }

        UpsertHelper.upsert_record(self, assoc_ids, FIELDS)
      end

      def remove_tag(book_id, tag)
        tag_id = UpsertHelper.normalize_id(tag)
        assoc_ids = { book_id: book_id, tag_id: tag_id }

        count = all.where(assoc_ids).delete_all
        return nil unless count == 1

        new(assoc_ids)
      end
    end
  end
end
