module Books
  module UpsertHelper
    module_function
    # Module used internally as a shared helper for `Books::BooksTag` and `Books::AuthorsBook`

    # Upsert entry in join table, using unique index specified by `fields`
    #
    # Returns a new instance of `klass` with join ids set when succesful.
    # Returns `nil` when upsert fails due to invalid foreign key values.
    #
    def upsert_record(klass, assoc_ids, fields)
      result = perform_upsert(klass, assoc_ids, fields)
      entry = result.to_a.first

      if entry
        klass.new(entry)
      else
        # Record already exists in db
        klass.new(assoc_ids)
      end

    rescue ActiveRecord::InvalidForeignKey
      nil
    end

    # Nest upsert operation within transaction
    #
    # When upsert operation fails, it poisons the pg transaction.
    # This would make tests which use transactional fixtures fail.
    # It would also make this operation harder to embed into
    # other transactions.
    #
    # Nesting within a transaction will allow parent transactions to
    # choose whether to rollback or continue on their own, after
    # inspecting the return value of this method.
    def perform_upsert(klass, entry, fields)
      klass.transaction do
        klass.upsert(entry, unique_by: fields, returning: fields)
      end
    end

    def normalize_id(record)
      case record
      when Numeric
        record
      when String
        Integer(record)
      else
        if record.respond_to?(:id)
          id = record.id
          unless id
            raise "Invalid associated record, id can\'t be nil: #{record.inspect}"
          end

          id
        else
          raise "Invalid associated record: #{record.inspect}"
        end
      end
    end
  end
end
