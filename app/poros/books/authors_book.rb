module Books
  class AuthorsBook < ApplicationRecord
    # Only used for upserting new join table records
    FIELDS = %i(author_id book_id).freeze

    ## Relationships
    belongs_to :book
    belongs_to :author

    class << self
      def add_author(book_id, author)
        raise 'Book id can\'t be blank' unless book_id.present?

        author_id = UpsertHelper.normalize_id(author)
        assoc_ids = { book_id: book_id, author_id: author_id }

        UpsertHelper.upsert_record(self, assoc_ids, FIELDS)
      end

      def remove_author(book_id, author)
        author_id = UpsertHelper.normalize_id(author)
        assoc_ids = { book_id: book_id, author_id: author_id }

        count = all.where(assoc_ids).delete_all
        return nil unless count == 1

        new(assoc_ids)
      end
    end
  end
end
