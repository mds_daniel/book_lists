# frozen_string_literal: true

class WikipediaUrlValidator < ActiveModel::EachValidator
  def validate_each(record, attribute, value)
    return false unless value.present?

    url = URI.parse(value)

    unless url.host == 'en.wikipedia.org'
      record.errors.add(attribute, 'must be a link to English Wikipedia')
      return false
    end

    unless url.scheme == 'https'
      record.errors.add(attribute, 'must have https as URI scheme')
      return false
    end

    true

  rescue URI::InvalidURIError
    record.errors.add(attribute, 'must be a valid URL')
    false
  end
end
