import { Controller } from 'stimulus'

export default class extends Controller {
  static targets = ["menu"];

  connect() {
    console.log('mounted in app');
  }

  toggleMenu(event) {
    event.preventDefault();
    this.menuTarget.classList.toggle('is-active');
  }
}
