class BooksTagsController < ApplicationController
  before_action :find_book

  def new
    authorize @book, :update?
    set_tag_select_options
  end

  def create
    authorize @book, :update?

    if @book.add_tag(params[:tag_id])
      flash[:notice] = 'Tag added successfully.'
      redirect_to book_path(@book)
    else
      set_tag_select_options
      flash.now[:alert] = 'Failed to add tag!'
      render :new
    end
  end

  def destroy
    authorize @book, :update?

    if @book.remove_tag(params[:id])
      flash[:notice] = 'Tag removed successfully.'
    else
      flash[:alert] = 'Failed to remove tag!'
    end

    redirect_to book_path(@book)
  end

  private

  def find_book
    @book = Book.find(params[:book_id])

  rescue ActiveRecord::RecordNotFound
    flash[:alert] = 'Book not found!'
    redirect_to books_path
  end

  def set_tag_select_options
    @tags = Tag.all
  end
end
