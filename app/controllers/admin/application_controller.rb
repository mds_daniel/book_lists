class Admin::ApplicationController < ApplicationController
  before_action :allow_only_admin
  skip_after_action :verify_authorized

  private

  def allow_only_admin
    unless user_signed_in? && current_user.admin?
      flash[:alert] = 'Admin area is restricted!'
      redirect_to root_path
    end
  end
end
