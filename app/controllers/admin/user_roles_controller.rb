class Admin::UserRolesController < Admin::ApplicationController
  before_action :find_user, only: :update

  def index
    @user_roles = User.user_roles
    @referred_user_id = params[:referred_user_id].to_i
  end

  def update
    if @user.update(user_role_params)
      flash[:notice] = 'User role updated successfully.'
      redirect_to admin_user_roles_path(referred_user_id: @user.id)
    else
      flash[:alert] = 'Failed to update user role!'
      redirect_to admin_user_roles_path
    end
  end

  private

  def find_user
    @user = User.find(params[:id])

  rescue ActiveRecord::RecordNotFound
    flash[:alert] = 'User not found!'
    redirect_to admin_user_roles_path
  end

  def user_role_params
    params.require(:user_role).permit(:role)
  end
end
