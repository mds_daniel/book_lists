class ApplicationController < ActionController::Base
  include Pundit
  rescue_from Pundit::NotAuthorizedError, with: :user_not_authorized
  after_action :verify_authorized, except: :index, unless: :devise_controller?

  before_action :authenticate_user!

  private

  def user_not_authorized
    flash[:alert] = 'You are not authorized to perform that action.'
    redirect_to root_path
  end
end
