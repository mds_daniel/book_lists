class TagsController < ApplicationController
  skip_before_action :authenticate_user!, only: [:index]
  before_action :find_tag, only: [:edit, :update, :destroy]
  rescue_from ActionController::ParameterMissing, with: :missing_tag_params

  def index
    @tags = Tag.for_index
  end

  def new
    @tag = Tag.new
    authorize @tag
  end

  def create
    @tag = Tag.new(tag_params)
    authorize @tag

    if @tag.save
      flash[:notice] = 'Tag created successfully.'
      redirect_to tags_path
    else
      flash.now[:alert] = 'Failed to create tag!'
      render :new
    end
  end

  def edit
    authorize @tag
  end

  def update
    authorize @tag

    if @tag.update(tag_params)
      flash[:notice] = 'Tag updated successfully.'
      redirect_to tags_path
    else
      flash.now[:alert] = 'Failed to update tag!'
      render :edit
    end
  end

  def destroy
    authorize @tag

    @tag.destroy

    flash[:notice] = 'Tag deleted successfully.'
    redirect_to tags_path
  end

  private

  def find_tag
    @tag = Tag.find(params[:id])

  rescue ActiveRecord::RecordNotFound
    flash[:alert] = 'Tag not found!'
    redirect_to tags_path
  end

  def tag_params
    params.require(:tag).permit(:name, :icon, :color)
  end

  def missing_tag_params
    flash[:alert] = 'Must supply tag params!'
    redirect_to new_tag_path
  end
end
