class BookReviewsController < ApplicationController
  include BookReviewPathHelper

  before_action :find_book
  before_action :find_book_review, only: [:edit, :update, :destroy]
  skip_before_action :authenticate_user!, only: :index

  rescue_from ActionController::ParameterMissing, with: :missing_book_review_params

  def index
    @book_reviews = @book.book_reviews.for_index.preload(:user)
    @referred_review_id = params[:book_review_id].to_i
  end

  def new
    @book_review = @book.book_reviews.new
    authorize @book_review
  end

  def create
    @book_review = @book.book_reviews.build(book_review_params)
    @book_review.user = current_user
    authorize @book_review

    if @book_review.save
      flash[:notice] = 'Book review created successfully.'
      redirect_to referred_book_review_path(@book, @book_review)
    else
      flash.now[:alert] = 'Failed to create book review!'
      render :new
    end
  end

  def edit
    authorize @book_review
  end

  def update
    authorize @book_review

    if @book_review.update(book_review_params)
      flash[:notice] = 'Book review updated successfully.'
      redirect_to referred_book_review_path(@book, @book_review)
    else
      flash.now[:alert] = 'Failed to update book review!'
      render :edit
    end
  end

  def destroy
    authorize @book_review

    @book_review.destroy!

    flash[:notice] = 'Book review deleted successfully.'
    redirect_to book_reviews_path(@book)
  end

  private

  def find_book
    @book = Book.find(params[:book_id])

  rescue ActiveRecord::RecordNotFound
    flash[:alert] = 'Book not found!'
    redirect_to books_path
  end

  def find_book_review
    @book_review = @book.book_reviews.find(params[:id])

    rescue ActiveRecord::RecordNotFound
      flash[:alert] = 'Book review not found!'
      redirect_to book_reviews_path(@book)
  end

  def book_review_params
    params.require(:book_review).permit(:body)
  end

  def missing_book_review_params
    flash[:alert] = 'Must supply book review params!'

    if action_name == 'create'
      redirect_to new_book_review_path(@book)
    else
      redirect_to edit_book_review_path(@book, @book_review)
    end
  end
end
