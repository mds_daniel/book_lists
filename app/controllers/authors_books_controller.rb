class AuthorsBooksController < ApplicationController
  before_action :find_book

  def new
    authorize @book, :update?
    set_author_select_options
  end

  def create
    authorize @book, :update?

    if @book.add_author(params[:author_id])
      flash[:notice] = 'Author added successfully.'
      redirect_to book_path(@book)
    else
      set_author_select_options
      flash.now[:alert] = 'Failed to add author!'
      render :new
    end
  end

  def destroy
    authorize @book, :update?

    if @book.remove_author(params[:id])
      flash[:notice] = 'Author removed successfully.'
    else
      flash[:alert] = 'Failed to remove author!'
    end

    redirect_to book_path(@book)
  end

  private

  def find_book
    @book = Book.find(params[:book_id])

  rescue ActiveRecord::RecordNotFound
    flash[:alert] = 'Book not found!'
    redirect_to books_path
  end

  def set_author_select_options
    @authors = Author.all.excluding_for_book(@book).only_id_and_name
  end
end
