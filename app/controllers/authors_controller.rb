class AuthorsController < ApplicationController
  skip_before_action :authenticate_user!, only: [:index, :show]
  before_action :find_author, only: [:show, :edit, :update, :destroy]
  rescue_from ActionController::ParameterMissing, with: :missing_author_params

  def index
    @authors = Author.for_index
  end

  def show
    authorize @author
  end

  def new
    @author = Author.new
    authorize @author
  end

  def create
    @author = Author.new(author_params)
    authorize @author

    if @author.save
      flash[:notice] = 'Author created successfully.'
      redirect_to author_path(@author)
    else
      flash.now[:alert] = 'Failed to create author!'
      render :new
    end
  end

  def edit
    authorize @author
  end

  def update
    authorize @author

    if @author.update(author_params)
      flash[:notice] = 'Author updated successfully.'
      redirect_to author_path(@author)
    else
      flash.now[:alert] = 'Failed to update author!'
      render :edit
    end
  end

  def destroy
    authorize @author

    @author.destroy!

    flash[:notice] = 'Author deleted successfully.'
    redirect_to authors_path
  end

  private

  def find_author
    @author = Author.find(params[:id])

  rescue ActiveRecord::RecordNotFound
    flash[:alert] = 'Author not found!'
    redirect_to authors_path
  end

  def author_params
    params.require(:author).permit(:name, :bio, :wiki_url)
  end

  def missing_author_params
    flash[:alert] = 'Must supply author params!'

    if action_name == 'create'
      redirect_to new_author_path
    else
      redirect_to edit_author_path(@author)
    end
  end
end
