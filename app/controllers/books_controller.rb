class BooksController < ApplicationController
  skip_before_action :authenticate_user!, only: [:index, :show]
  before_action :find_book, only: [:show, :edit, :update, :destroy]
  rescue_from ActionController::ParameterMissing, with: :missing_book_params

  def index
    @search_params = search_params
    @books = Book.search_for_index(@search_params)
                 .includes(:authors, :tags)

    @user_reviews = BookReview.for_user(current_user)
                              .index_by(&:book_id)

    load_search_form_select_options
  end

  def show
    authorize @book

    @authors = @book.authors.only_id_and_name
    @tags = @book.tags.to_a
    @book_review = BookReview.find_by(user: current_user, book: @book) if current_user
  end

  def new
    @book = Book.new
    authorize @book
    load_author_select_options
  end

  def create
    @book = Book.new(book_params)
    authorize @book

    if @book.save_with_new_author(params[:author_id])
      flash[:notice] = 'Book created successfully.'
      redirect_to book_path(@book)
    else
      load_author_select_options
      flash.now[:alert] = 'Failed to create book!'
      render :new
    end
  end

  def edit
    authorize @book
  end

  def update
    authorize @book

    if @book.update(book_params)
      flash[:notice] = 'Book updated successfully.'
      redirect_to book_path(@book)
    else
      flash.now[:alert] = 'Failed to update book!'
      render :edit
    end
  end

  def destroy
    authorize @book

    @book.destroy!

    flash[:notice] = 'Book deleted successfully.'
    redirect_to books_path
  end

  private

  def find_book
    @book = Book.find(params[:id])

  rescue ActiveRecord::RecordNotFound
    flash[:alert] = 'Book not found!'
    redirect_to books_path
  end

  def search_params
    params.fetch(:search) { ActionController::Parameters.new }
          .permit(:title, :author_id, :tag_id)
  end

  def book_params
    params.require(:book).permit(:title, :description)
  end

  def load_search_form_select_options
    load_author_select_options
    load_tag_select_options
  end

  def load_author_select_options
    @authors = Author.all.only_id_and_name
  end

  def load_tag_select_options
    @tags = Tag.all
  end

  def missing_book_params
    flash[:alert] = 'Must supply book params!'

    if action_name == 'create'
      redirect_to new_book_path
    else
      redirect_to edit_book_path(@book)
    end
  end
end
