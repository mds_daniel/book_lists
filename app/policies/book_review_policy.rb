class BookReviewPolicy < ApplicationPolicy
  def show?
    true
  end

  def create?
    !user.nil?
  end

  def update?
    belongs_to_user?
  end

  def destroy?
    admin? || belongs_to_user?
  end

  private

  def belongs_to_user?
    return false unless user
    record.user_id == user.id
  end
end
