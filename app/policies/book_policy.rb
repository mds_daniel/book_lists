class BookPolicy < ApplicationPolicy
  def show?
    true
  end

  def create?
    admin_or_staff?
  end

  def update?
    admin_or_staff?
  end

  def destroy?
    admin?
  end
end
