# frozen_string_literal: true

module ApplicationHelper
  def signed_in_as_staff?
    user_signed_in? && current_user.staff?
  end

  def navbar_brand(label)
    css_class = ['navbar-item']
    css_class << 'is-active' if current_page?(root_path)

    link_to label, root_path, class: css_class
  end

  def navbar_link(label, path, icon_class)
    css_class = ['navbar-item']
    css_class << 'is-active' if current_page?(path)

    link_to path, class: css_class do
      concat(tag.span(class: 'icon') do
        tag.i class: icon_class, aria: { hidden: 'true' }
      end)
      concat(tag.span label)
    end
  end
end
