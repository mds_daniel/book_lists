# frozen_string_literal: true

module TagsViewHelper
  def display_tag(tag)
    content_tag(:div, id: "book-tag-#{tag.id}",
                      style: display_tag_css_style(tag),
                      class: 'tag') do
      concat(icon_span(tag.icon)) if tag.icon
      concat(content_tag(:span, tag.name))
    end
  end

  def display_tag_css_style(tag)
    return '' unless tag.color.present?

    "background-color: #{tag.color}; color: #fff"
  end
end
