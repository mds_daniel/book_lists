# frozen_string_literal: true

module IconViewHelper
  module_function

  FA_ICONS = [
    'fas fa-dragon',
    'fas fa-skull-crossbones',
    'fas fa-scroll',
    'fas fa-hat-wizad',
    'fas fa-dungeon',
    'fas fa-space-shuttle',
    'fas fa-user-astronaut',
    'fas fa-meteor',
    'fas fa-rocket',
    'fas fa-mortar-pestle',
    'fas fa-newspaper',
    'fas fa-bacon',
    'fas fa-pizza-slice',
    'fas fa-golf-ball',
    'fas fa-user-tie',
    'fas fa-campground',
    'fas fa-tooth',
    'fas fa-bowling-ball',
    'fas fa-utensils',
    'fas fa-chess-queen',
    'fas fa-ghost',
    'fas fa-spider',
    'fas fa-cloud-moon',
  ].freeze

  def tag_icon
    FA_ICONS.sample
  end

  def icon_span(icon)
    tag.span class: 'icon' do
      tag.i class: icon, aria: { hidden: true }
    end
  end

  def tag_icon_options_list
    content_tag(:datalist, tag_icon_options, id: 'tag-icon-options-list')
  end

  def tag_icon_options
    @_tag_icon_options ||= safe_join(build_tag_icon_options).freeze
  end

  private

  def build_tag_icon_options
    FA_ICONS.map do |icon_name|
      content_tag(:option, icon_name)
    end
  end
end
