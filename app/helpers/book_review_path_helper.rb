module BookReviewPathHelper
  private

  # Generates a path to the book_review, using it's id as an anchor.
  # Used in both `BookReviewsController` and views
  def referred_book_review_path(book, user_book_review)
    anchor = "book-review-#{user_book_review.id}"
    book_reviews_path(book, book_review_id: user_book_review.id, anchor: anchor)
  end
end
