# frozen_string_literal: true

module FormHelper
  def help_error_message(form, field)
    messages = form.object.errors.full_messages_for(field)

    if messages.any?
      content_tag(:span, messages.join('. '), class: 'help is-danger')
    end
  end
end
