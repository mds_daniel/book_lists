# frozen_string_literal: true

module BooksViewHelper
  def book_reviews_link(book)
    noun = pluralize(book.book_reviews_count, 'book review',
      plural: 'book reviews')

    link_to "#{noun} so far", book_reviews_path(book),
      class: 'button is-ghost'
  end

  def book_review_policy(book)
    review = BookReview.new(user: current_user, book: book)
    BookReviewPolicy.new(current_user, review)
  end

  def books_by_author_link(author)
    books_path(search: { author_id: author.id })
  end
end
