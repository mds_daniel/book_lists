# frozen_string_literal: true

module UserRolesViewHelper
  def opposite_user_role(user_role)
    user_role.visitor? ? 'staff' : 'visitor'
  end

  def update_user_role_button(user_role)
    opts = {
      id: "user-role-#{user_role.user_id}-update",
      type: 'submit',
    }

    label, css_class =
      if user_role.visitor?
        ['Upgrade to Staff', 'is-primary']
      else
        ['Downgrade to Visitor', 'is-danger']
      end

    opts[:class] = ['button', 'is-small', css_class]

    content_tag(:button, label, opts)
  end

  def user_role_row(user_role, referred_id)
    opts = { id: "user-role-#{user_role.user_id}" }

    if user_role.user_id == referred_id
      opts[:class] = 'is-primary'
    end

    content_tag(:tr, opts) do
      yield
    end
  end
end
