Rails.application.routes.draw do
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
  root to: 'welcome#index'
  devise_for :users

  resources :books do
    resources :book_reviews, as: :reviews, except: [:show]
    resources :authors_books, as: :authors, only: [:new, :create, :destroy]
    resources :books_tags, as: :tags, only: [:new, :create, :destroy]
  end

  resources :authors
  resources :tags, except: [:show]

  namespace :admin do
    resources :user_roles, only: [:index, :update]
  end
end
